#!/usr/bin/env bash
aws s3 sync --acl public-read --profile myNextMovie --delete ./dist/ s3://cloud.my-next-movie.com;
aws cloudfront --profile myNextMovie create-invalidation --distribution-id E3FZLDND70FMQT --paths '/*'