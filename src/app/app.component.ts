import { Component } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/mergeMapTo';
import {AuthService} from './services/auth.service';
import * as collectionActions from './ngrx-actions/collection.actions';
import * as reactionsActions from './ngrx-actions/reactions.actions';
import * as recommendationsActions from './ngrx-actions/recommendations.actions';
import {Store} from '@ngrx/store';
import {MoviesService} from './services/movies.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {RegisterDialogComponent} from './shared/register-dialog/register-dialog.component';
import {LoadUser, LoginEmail, Logout, Register, CheckAuth} from './ngrx-actions/auth.actions';
import * as state from './ngrx-reducers';
import {LoginDialogComponent} from './shared/login-dialog/login-dialog.component';
import {getReactionsCounterCount, getRecommendations, getRecommendationsIds} from './ngrx-reducers/index';
import {EVERY_REACTIONS, MIN_REACTIONS} from './ngrx-reducers/reactions-counter.reducer';
import { timer } from 'rxjs/observable/timer';
import {PopupService} from './services/popup.service';
import {IPopupData} from './models/IPopupData';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public profileData$: Observable<any>;
    public popupData$: Observable<IPopupData>;
    private checkInterval;
    constructor(private store: Store<state.State>,
                public dialog: MatDialog, private snackBar: MatSnackBar, private popupService: PopupService) {

        if (!AuthService.isAuthenticated()) {
            this.store.dispatch(new Logout());
        }

        // this.store.dispatch(new LoadUser());
        this.profileData$ = this.store.select(state.getAuthUser);

        this.profileData$.subscribe(user => {
            if (user) {
                this.handleSuccess('Authenticated successfully');
                this.store.dispatch(new collectionActions.Load({}));
                this.store.dispatch(new recommendationsActions.Load({}));
                this.store.dispatch(new reactionsActions.LoadLikes({}));
                this.store.dispatch(new reactionsActions.LoadDislikes({}));
                this.store.dispatch(new reactionsActions.CountReactions());
                this.checkInterval = setInterval(() => this.store.dispatch(new CheckAuth()), 10000);
            } else {
              clearInterval(this.checkInterval);
            }
        });

        this.store.select(getReactionsCounterCount).subscribe(num => {
          if (num >= MIN_REACTIONS && num % EVERY_REACTIONS === 0) {
            this.store.dispatch(new recommendationsActions.Clear());
            this.store.dispatch(new recommendationsActions.Load());
            timer(0, 2000).withLatestFrom(this.store.select(getRecommendationsIds)).subscribe(([time, ids]) => {
              if (ids.length === 0) {
                this.store.dispatch(new recommendationsActions.Load());
              }
            });
          }
        });

        this.store.select(state.getAuthError).subscribe(err => {
            this.handleError(err);
        });

        this.popupData$ = this.popupService.popup$;
    }

    onInfoEnter() {
        this.popupService.mouseOnPanel(true);
    }

    onInfoLeave() {
      this.popupService.mouseOnPanel(false);
    }

    onFacebookLogin() {
        console.log('Login');
    }

    onLogin() {
        const dialogRef = this.dialog.open(LoginDialogComponent, {
            width: '400px'
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed', result);
            if (result) {
                this.store.dispatch(new LoginEmail(result));
            }
        });
    }

    onLogout() {
        this.store.dispatch(new Logout());
    }

    onRegister() {
        const dialogRef = this.dialog.open(RegisterDialogComponent, {
            width: '400px'
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed', result);
            if (result) {
                this.store.dispatch(new Register(result));
            }
        });
    }

    handleSuccess(msg: string) {
        if (!msg || msg.length === 0) return;
        this.snackBar.open(msg, '', {
            duration: 2000,
            panelClass: 'success-popup'
        });
    }

    handleError(msg: string) {
        if (!msg || msg.length === 0) return;
        this.snackBar.open(msg, '', {
            duration: 2000,
            panelClass: 'error-popup'
        });
    }
}
