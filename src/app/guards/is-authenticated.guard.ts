import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import * as state from '../ngrx-reducers';
import {AuthService} from '../services/auth.service';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate {

    constructor(private store: Store<state.State>, private router: Router) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        routerState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        const loggedIn$ = this.store.select(state.getAuthLoggedIn);

        // redirect to sign in page if user is not authenticated
        loggedIn$.subscribe(auth => {
            if (!auth) {
                this.router.navigate(['login'], { queryParams: { returnUrl: routerState.url }});
            }
        });

        return loggedIn$;
    }
}
