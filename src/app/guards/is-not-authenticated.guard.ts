import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import * as state from '../ngrx-reducers';
import {AuthService} from '../services/auth.service';

@Injectable()
export class IsNotAuthenticatedGuard implements CanActivate {

    constructor(private store: Store<state.State>, private router: Router) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        routerState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        const notLoggedIn$ = this.store.select(state.getAuthLoggedIn).map(loggedIn => !loggedIn);

        // redirect to home page if user is authenticated
        notLoggedIn$.subscribe(notAuthenticated => {
            if (!notAuthenticated) {
                this.router.navigate(['']);
            }
        });

        return notLoggedIn$;
    }
}
