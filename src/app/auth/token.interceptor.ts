/**
 * Created by baunov on 07/11/2017.
 */
import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {AuthService} from '../services/auth.service';
import {HttpHeaders} from '@angular/common/http';
import {BASE_URL} from '../api';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url.indexOf(BASE_URL) > -1) {
            const outReq = request.clone({setHeaders: {'x-auth-token': localStorage.getItem('id_token') || ''}});
            return next.handle(outReq);
        }
        return next.handle(request);
    }
}
