import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './modules/home/home-page/home-page.component';
import {MoviePageComponent} from './modules/movie/movie-page/movie-page.component';
import {CollectionPageComponent} from './modules/collection/collection-page/collection-page.component';
import {RecommendationsPageComponent} from './modules/recommendations/recommendations-page/recommendations-page.component';
import {ActorPageComponent} from './modules/actor/actor-page/actor-page.component';
import {GenrePageComponent} from './modules/genre/genre-page/genre-page.component';
import {IsAuthenticatedGuard} from './guards/is-authenticated.guard';
import {IsNotAuthenticatedGuard} from './guards/is-not-authenticated.guard';
import {LoginPageComponent} from './modules/login/login-page/login-page.component';
import {ReactionsPageComponent} from './modules/reactions/reactions-page/reactions-page.component';


const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'search/:search', component: HomePageComponent },
  { path: 'movie/:id', component: MoviePageComponent },
  { path: 'actor/:name', component: ActorPageComponent },
  { path: 'genre/:name', component: GenrePageComponent },
  { path: 'collection', canActivate: [IsAuthenticatedGuard], component: CollectionPageComponent },
  { path: 'recommendations', canActivate: [IsAuthenticatedGuard], component: RecommendationsPageComponent },
  { path: 'reactions', canActivate: [IsAuthenticatedGuard], component: ReactionsPageComponent },
  { path: 'login', canActivate: [IsNotAuthenticatedGuard], component: LoginPageComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
