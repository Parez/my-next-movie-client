import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as state from '../../ngrx-reducers/';
import {IMovie} from '../../models/IMovie';
import {ListDomain} from '../../ngrx-actions/movies-list.actions';

@Component({
    selector: 'app-bottom-loader',
    templateUrl: './bottom-loader.component.html',
    styleUrls: ['./bottom-loader.component.scss']
})
export class BottomLoaderComponent implements OnInit {

    @Input() listDomain: ListDomain;
    public loading$: Observable<boolean>;
    public loaded$: Observable<boolean>;
    public allLoaded$: Observable<boolean>;
    public foundMovies$: Observable<IMovie[]>;

    public noMovies$: Observable<boolean>;
    public noMoviesFound$: Observable<boolean>;
    public reachedBottom$: Observable<boolean>;

    public isSearching$: Observable<boolean>;


    constructor(private store: Store<state.State>) {

    }

    ngOnInit() {
        console.log(this.listDomain);
        switch (this.listDomain) {
            case(ListDomain.SEARCH): {
                this.foundMovies$ = this.store.select(state.getSearchResults);
                this.loading$ = this.store.select(state.getSearchLoading);
                this.loaded$ = this.store.select(state.getSearchLoaded);
                this.allLoaded$ = this.store.select(state.getSearchAllLoaded);
                break;
            }
            case(ListDomain.GENRE): {
                this.foundMovies$ = this.store.select(state.getGenreResults);
                this.loading$ = this.store.select(state.getGenreLoading);
                this.loaded$ = this.store.select(state.getGenreLoaded);
                this.allLoaded$ = this.store.select(state.getGenreAllLoaded);
                break;
            }
            case(ListDomain.ACTOR): {
                this.foundMovies$ = this.store.select(state.getActorResults);
                this.loading$ = this.store.select(state.getActorLoading);
                this.loaded$ = this.store.select(state.getActorLoaded);
                this.allLoaded$ = this.store.select(state.getActorAllLoaded);
                break;
            }
            case(ListDomain.COLLECTION): {
                this.foundMovies$ = this.store.select(state.getCollectionMovies);
                this.loading$ = this.store.select(state.getCollectionLoading);
                this.loaded$ = this.store.select(state.getCollectionLoaded);
                this.allLoaded$ = this.store.select(state.getCollectionAllLoaded);
                break;
            }
        }

        this.isSearching$ = this.loading$.withLatestFrom(this.loaded$, (loading, loaded) => {
            return (loading && !loaded) || (!loading && loaded);
        });

        // No movies currently
        this.noMoviesFound$ = this.foundMovies$.map(movies => movies.length === 0);

        // No movies and nothing is loading
        this.noMovies$ = this.loading$.withLatestFrom(
            this.noMoviesFound$,
            this.isSearching$,
            this.allLoaded$,
            (loading, noMovies, isSearching, allLoaded) => {
                console.log(loading, noMovies, isSearching, allLoaded);
                return !loading && noMovies && isSearching;
            }
        );

        this.reachedBottom$ = this.loading$.withLatestFrom(this.allLoaded$, this.noMoviesFound$, (loading, allLoaded, noMoviesFound) => {
            console.log(allLoaded, !noMoviesFound);
            return allLoaded && !noMoviesFound;
        });
    }

}
