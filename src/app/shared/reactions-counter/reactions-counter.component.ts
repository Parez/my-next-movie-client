import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as state from '../../ngrx-reducers';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-reactions-counter',
  templateUrl: './reactions-counter.component.html',
  styleUrls: ['./reactions-counter.component.scss'],
})
export class ReactionsCounterComponent implements OnInit {

  numReactions$: Observable<number>;
  numReactionsLeft$: Observable<number>;
  constructor(private store: Store<state.State>) {
    this.numReactions$ = store.select(state.getReactionsCounterCount);
    this.numReactionsLeft$ = store.select(state.getReactionsCounterCountLeft);
  }

  ngOnInit() {
  }

}
