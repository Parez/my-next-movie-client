import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {AuthService} from '../services/auth.service';
import {TokenInterceptor} from '../auth/token.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { BottomLoaderComponent } from './bottom-loader/bottom-loader.component';
import {ImgFallbackModule} from 'ngx-img-fallback';
import {IsAuthenticatedGuard} from '../guards/is-authenticated.guard';
import {IsNotAuthenticatedGuard} from '../guards/is-not-authenticated.guard';
import {LoginDialogComponent} from './login-dialog/login-dialog.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {RegisterDialogComponent} from './register-dialog/register-dialog.component';
import {RegisterFormComponent} from './register-form/register-form.component';
import { ReactionsCounterComponent } from './reactions-counter/reactions-counter.component';
import {MoviesService} from '../services/movies.service';
import { PopupMovieInfoComponent } from './popup-movie-info/popup-movie-info.component';
import { RateMovieComponent } from './rate-movie/rate-movie.component';
import {YoutubeModule} from 'angularx-youtube';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        InfiniteScrollModule,
        YoutubeModule,
        ImgFallbackModule
    ],
    exports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        InfiniteScrollModule,
        YoutubeModule,
        BottomLoaderComponent,
        ImgFallbackModule,
        LoginDialogComponent,
        LoginFormComponent,
        RegisterDialogComponent,
        RegisterFormComponent,
        ReactionsCounterComponent,
        PopupMovieInfoComponent,
        RateMovieComponent
    ],
    declarations: [
        BottomLoaderComponent,
        LoginDialogComponent,
        LoginFormComponent,
        RegisterDialogComponent,
        RegisterFormComponent,
        ReactionsCounterComponent,
        PopupMovieInfoComponent,
        RateMovieComponent
    ],
    providers: [
        IsAuthenticatedGuard,
        IsNotAuthenticatedGuard,
        AuthService,
        MoviesService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
    ],
    entryComponents: [
        LoginDialogComponent,
        RegisterDialogComponent
    ]
})
export class SharedModule { }
