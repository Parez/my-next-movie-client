import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {EMAIL} from '../../utils/RegexPatterns';
import {AuthService} from '../../services/auth.service';
import {IEmailPasswordCredentials} from '../../models/IEmailPasswordCredentials';
import {Output, EventEmitter} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  @Output() submitted: EventEmitter<IEmailPasswordCredentials> = new EventEmitter<IEmailPasswordCredentials>();
  emailError: string;
  passwordError: string;
  form: FormGroup;

  constructor(private fb: FormBuilder, private authSevice: AuthService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL)])],
      password: ['', Validators.required]
    });

    const emailStream$ = this.form.controls['email'].valueChanges;
      const passwordStream$ = this.form.controls['password'].valueChanges;

    emailStream$.subscribe( (data) => {
      this.emailErrors(data);
    });
    passwordStream$.subscribe( (data) => {
      this.passwordErrors(data);
    });

  }

  emailErrors(value) {
    if (value.length === 0) {
      this.emailError = 'Email is required';
    } else if (!this.form.controls['email'].valid) {
      this.emailError = 'Email is not valid';
    } else {
      this.emailError = '';
    }
  }

  passwordErrors(value) {
    this.passwordError = '';
    if (value.length === 0) {
      this.passwordError = 'Password is required';
    }
  }

  onError(msg: string) {
      this.snackBar.open(msg, '', {
          duration: 1000,
      });
  }

  onSubmit() {
    if (this.form.valid) {
      const email = this.form.controls['email'].value;
      const password = this.form.controls['password'].value;
      const cred: IEmailPasswordCredentials = {email, password};
      this.submitted.emit(cred);
    } else {
      this.onError('Form validation failed');
    }
  }

}
