import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {IRegisterCredentials} from '../../models/IRegisterCredentials';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss']
})
export class RegisterDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RegisterDialogComponent>) { }

  ngOnInit() {
  }

  onSubmit(data: IRegisterCredentials) {
      this.dialogRef.close(data);
  }

}
