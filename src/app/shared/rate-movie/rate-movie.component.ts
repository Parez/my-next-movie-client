import {Component, Input, Output, EventEmitter, ViewChild, AfterViewInit, ElementRef} from '@angular/core';
import {IMovie} from '../../models/IMovie';
import {Store} from '@ngrx/store';
import * as state from '../../ngrx-reducers';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/merge';
import * as ReactionsActions from '../../ngrx-actions/reactions.actions';

@Component({
  selector: 'app-rate-movie',
  templateUrl: './rate-movie.component.html',
  styleUrls: ['./rate-movie.component.scss']
})
export class RateMovieComponent implements AfterViewInit {
  @ViewChild('likeBtn', { read: ElementRef }) likeBtn: ElementRef;
  @ViewChild('dislikeBtn', { read: ElementRef }) dislikeBtn: ElementRef;

  @Output() liked: EventEmitter<void> = new EventEmitter<void>();
  @Output() disliked: EventEmitter<void> = new EventEmitter<void>();
  @Input() set movie(val: IMovie) {
    this._movie = val;
    this.store.select(state.getMovieReaction(val.imdbID.toString())).subscribe(reaction => {
      this.reaction = reaction;
    });
    this.store.dispatch(new ReactionsActions.LoadMovieReaction({movie: val}));
  }
  private _movie: IMovie;

  error$: Observable<string>;
  reaction: number;
  isAuthenticated$: Observable<boolean>;
  likeClicks$: Observable<any>;
  dislikeClicks$: Observable<any>;
  constructor(private store: Store<state.State>) {
    this.error$ = this.store.select(state.getReactionsError);
    this.isAuthenticated$ = this.store.select(state.getAuthLoggedIn);
  }

  ngAfterViewInit(): void {
    if (this.likeBtn && this.dislikeBtn) {
      this.likeClicks$ = Observable.fromEvent(this.likeBtn.nativeElement, 'click');
      this.dislikeClicks$ = Observable.fromEvent(this.dislikeBtn.nativeElement, 'click');

      this.likeClicks$.mapTo(1).merge(this.dislikeClicks$.mapTo(-1)).debounceTime(1500).subscribe((reaction) => {
        if (reaction > 0) {
          this.store.dispatch(new ReactionsActions.Like({movie: this._movie}));
        } else {
          this.store.dispatch(new ReactionsActions.Dislike({movie: this._movie}));
        }
      });
    }
  }

  onLike(): void {
    if (this._movie) {
      this.reaction = 1;
      this.liked.emit();
    }
  }

  onDislike(): void {
    if (this._movie) {
      this.reaction = -1;
      this.disliked.emit();
    }
  }

}
