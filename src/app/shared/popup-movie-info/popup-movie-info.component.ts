import { Component, OnInit, Input, ElementRef } from '@angular/core';
import {IPopupData} from '../../models/IPopupData';
import {Router} from '@angular/router';

const WIDTH: number = 300;
const HEIGHT: number = 300;

@Component({
  selector: 'app-popup-movie-info',
  templateUrl: './popup-movie-info.component.html',
  styleUrls: ['./popup-movie-info.component.scss'],
})
export class PopupMovieInfoComponent implements OnInit {

  @Input() set data(val: IPopupData) {
    console.log(val);
    this._data = val;
    if (val !== null) {
      this.elRef.nativeElement.style['opacity'] = 0.9;
      const elem = this.elRef.nativeElement;
      const tgX = val.posRight;
      const tgY = val.posTop;

      elem.style.width = val.width.toString() + 'px';
      elem.style.height = val.height.toString() + 'px';

      elem.style.left = (tgX - val.width).toString() + 'px';
      elem.style.top = (tgY - 60).toString() + 'px';

      /*console.log(tgX + WIDTH, window.innerWidth);
      if (tgX + WIDTH >= window.innerWidth) {
        tgX = val.posLeft - WIDTH - 50;
      }
      console.log(tgY + HEIGHT - window.scrollY, window.innerHeight);
      if (tgY + HEIGHT - window.scrollY >= window.innerHeight) {
        tgY -= HEIGHT + 30;
      }
      elem.style.left = tgX.toString() + 'px';
      elem.style.top = (tgY - 60).toString() + 'px';*/
    } else {
      this.elRef.nativeElement.style['opacity'] = 0;
    }
  }
  get data(): IPopupData {
    return this._data;
  }
  private _data: IPopupData = null;

  constructor(private router: Router, private elRef: ElementRef) {
    this.elRef.nativeElement.style.position = 'absolute';
    this.elRef.nativeElement.style['z-index'] = 2000;
    this.elRef.nativeElement.style['opacity'] = 0;
  }

  ngOnInit() {
  }

  navigateToActor(name: string): void {
    this.router.navigate(['actor', name]);
  }

  navigateToGenre(name: string): void {
    this.router.navigate(['genre', name]);
  }

}
