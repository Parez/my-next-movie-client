import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {EMAIL, STRONG_PASS, MEDIUM_PASS} from '../../utils/RegexPatterns';
import {IRegisterCredentials} from '../../models/IRegisterCredentials';
import {AuthService} from '../../services/auth.service';
import {Output, EventEmitter} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  @Output() submitted: EventEmitter<IRegisterCredentials> = new EventEmitter<IRegisterCredentials>();

  minUsernameLength: number = 4;
  maxUsernameLength: number = 16;
  minPasswordLength: number = 6;

  emailError: string;
  passwordError: string;
  usernameError: string;

  form: FormGroup;

  passStrength: number;

  constructor(private fb: FormBuilder, private userService: AuthService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(this.minPasswordLength)])],
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(this.minUsernameLength),
        Validators.maxLength(this.maxUsernameLength)])]
    });

    const emailStream$ = this.form.controls['email'].valueChanges;
    const passwordStream$ = this.form.controls['password'].valueChanges;
    const usernameStream$ = this.form.controls['username'].valueChanges;

    emailStream$.subscribe( (data) => {
      this.emailErrors(data);
    });
    usernameStream$.subscribe( (data) => {
      this.usernameErrors(data);
    });
    passwordStream$.subscribe( (data) => {
      this.passwordErrors(data);
    });
  }

  emailErrors(value) {
    if (value.length === 0) {
      this.emailError = 'Email is required';
    } else if (!this.form.controls['email'].valid) {
      this.emailError = 'Email is not valid';
    } else {
      this.emailError = '';
    }
  }

  usernameErrors(value) {
    if (value.length === 0) {
      this.usernameError = 'Username is required';
    } else if (value.length < this.minUsernameLength) {
      this.usernameError = `Username is too short (${this.minUsernameLength} characters minimum)`;
    } else if (value.length > this.maxUsernameLength) {
      this.usernameError = `Username is too long (${this.maxUsernameLength} characters maximum)`;
    } else {
      this.usernameError = '';
    }
  }

  passwordErrors(value) {
    this.passwordError = '';
    if (value.length === 0) {
      this.passwordError = 'Password is required';
    } else if (value.length < this.minPasswordLength) {
      this.passwordError = 'Password is too short';
    }
  }

  onError(msg: string) {
      this.snackBar.open(msg, '', {
          duration: 1000,
      });
  }

  onSubmit() {
    if (this.form.valid) {
      const cred: IRegisterCredentials = {
          username: this.form.controls['username'].value,
          email: this.form.controls['email'].value,
          password: this.form.controls['password'].value
      };
      this.submitted.emit(cred);
    } else {
      this.onError('Form validation failed');
    }
  }

}
