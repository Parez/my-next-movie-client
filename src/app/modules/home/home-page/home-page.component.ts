import {Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MoviesService } from '../../../services/movies.service';
import { IMovie } from '../../../models/IMovie';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material';
import { IMovieBase } from '../../../models/IMovieBase';
import { Store } from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import * as movieActions from '../../../ngrx-actions/search.actions';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {AMOUNT_PER_PAGE} from '../../../ngrx-reducers/movies-list.reducer';
import {ListDomain} from '../../../ngrx-actions/movies-list.actions';
import 'rxjs/add/operator/take';

@Component({
    selector: 'app-home',
    templateUrl: 'home-page.component.html',
    styleUrls: ['home-page.component.scss']
})
export class HomePageComponent implements OnInit, AfterViewInit {

    @ViewChild(MatAutocompleteTrigger)
    private autoComplete: MatAutocompleteTrigger;

    public form: FormGroup;
    public foundMovies$: Observable<IMovie[]>;
    public loading$: Observable<boolean>;
    public searchOptions$: Observable<IMovieBase[]>;
    public allLoaded$: Observable<boolean>;
    public searchQuery: string = '';
    public listDomain: ListDomain = ListDomain.SEARCH;
    public sortBy = [
        {value: '', viewValue: 'Relevance'},
        {value: 'imdbRating', viewValue: 'Rating'},
        {value: 'Released', viewValue: 'Release Date'}
    ];

    public sortValue: string = '';

    constructor(private fb: FormBuilder,
                private store: Store<state.State>,
                private router: Router,
                private snackBar: MatSnackBar,
                private route: ActivatedRoute) {

        this.form = this.fb.group({
            movieName: [this.searchQuery, Validators.minLength(3)],
        });

        this.loading$ = store.select(state.getSearchLoading);
        this.allLoaded$ = store.select(state.getSearchAllLoaded);
        this.foundMovies$ = store.select(state.getSearchResults);
        store.select(state.getSearchQuery).map(query => query.search).subscribe((query) => {
            this.searchQuery = query;
            console.log(query);
            this.form.controls['movieName'].setValue(this.searchQuery);
        });

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.searchQuery = params.get('search');
            this.updateSearch();
            this.form.controls['movieName'].setValue(this.searchQuery);
        });

    }

    ngAfterViewInit(): void {

    }

    onSortChange(sortOn): void {
        this.sortValue = sortOn.value;
        this.updateSearch();
    }

    private updateSearch() {
        if (this.searchQuery && this.searchQuery.length > 0) {
            this.store.dispatch(new movieActions.Search({
                search: this.searchQuery,
                limit: AMOUNT_PER_PAGE,
                field: 'SearchField',
                sortOn: this.sortValue
            }));
        }
    }

    ngOnInit() {
        this.store.select(state.getSearchQuery).take(1).subscribe(searchQuery => {
            if (searchQuery.search.length > 0) return;
            this.store.dispatch(new movieActions.Search({
                limit: AMOUNT_PER_PAGE,
                sortOn: 'Released',
                sortOrder: -1,
                field: '',
                search: ''
            }));
        });
    }

    selectMovie(event) {
        this.router.navigate(['/movie', event.option.value.imdbID]);
    }

    onSearch(event) {
        if (event.keyCode !== 13) {
            return;
        }
        const searchText = this.form.controls['movieName'].value;
        if (searchText.length >= 3) {
            // this.autoComplete.closePanel();
            // this.store.dispatch(new movieActions.Search({search: searchText, limit: AMOUNT_PER_PAGE, field: 'SearchField'}));
            this.router.navigate([`search/${searchText}`]);
        } else {
            this.snackBar.open('Query is too short. Type at least 3 symbols in search bar', '', {
                duration: 1000,
            });
        }
    }

    onScroll() {
        this.store.dispatch(new movieActions.SearchMore({limit: AMOUNT_PER_PAGE}));
    }

}
