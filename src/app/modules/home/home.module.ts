import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import {MaterialModule} from '../../material.module';
import {SharedModule} from '../../shared/shared.module';
import {MovieModule} from '../movie/movie.module';

@NgModule({
  imports: [
      SharedModule,
      MovieModule
  ],
  declarations: [
      HomePageComponent
  ],
  exports: [
      HomePageComponent
  ]

})
export class HomeModule { }
