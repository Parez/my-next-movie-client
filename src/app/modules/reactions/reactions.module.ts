import { NgModule } from '@angular/core';
import { ReactionsPageComponent } from './reactions-page/reactions-page.component';
import {MovieModule} from '../movie/movie.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    MovieModule,
    SharedModule
  ],
  declarations: [ReactionsPageComponent]
})
export class ReactionsModule { }
