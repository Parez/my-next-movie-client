import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IMovie} from '../../../models/IMovie';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {getDislikedMovies, getLikedMovies, getReactionsLoading} from '../../../ngrx-reducers/index';

@Component({
  selector: 'app-reactions-page',
  templateUrl: './reactions-page.component.html',
  styleUrls: ['./reactions-page.component.scss'],
})
export class ReactionsPageComponent implements OnInit {

  likes$: Observable<IMovie[]>;
  dislikes$: Observable<IMovie[]>;

  loading$: Observable<boolean>

  constructor(private store: Store<state.State>) {
    this.likes$ = this.store.select(getLikedMovies);
    this.dislikes$ = this.store.select(getDislikedMovies);

    this.loading$ = this.store.select(getReactionsLoading);
  }

  ngOnInit() {
  }

}
