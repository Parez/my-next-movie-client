import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollectionPageComponent } from './collection-page/collection-page.component';
import { CollectionComponent } from './collection.component';
import {SharedModule} from '../../shared/shared.module';
import {MovieModule} from '../movie/movie.module';
import {CollectionService} from '../../services/collection.service';

@NgModule({
  imports: [
    SharedModule,
    MovieModule
  ],
  declarations: [CollectionPageComponent, CollectionComponent],
  providers: [
      CollectionService
  ]
})
export class CollectionModule { }
