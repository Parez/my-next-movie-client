import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as state from '../../ngrx-reducers';
import {Observable} from 'rxjs/Observable';
import {IMovie} from '../../models/IMovie';
import * as collectionActions from '../../ngrx-actions/collection.actions';
import {ListDomain} from '../../ngrx-actions/movies-list.actions';

@Component({
    selector: 'app-collection',
    templateUrl: './collection.component.html',
    styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

    public listDomain: ListDomain = ListDomain.COLLECTION;
    public movies$: Observable<IMovie[]>;
    constructor(private store: Store<state.State>) {
        this.movies$ = store.select(state.getCollectionMovies);
    }

    ngOnInit() {
    }

}
