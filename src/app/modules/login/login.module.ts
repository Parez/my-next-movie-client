import { NgModule } from '@angular/core';
import { LoginPageComponent } from './login-page/login-page.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [LoginPageComponent]
})
export class LoginModule { }
