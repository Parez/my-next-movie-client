import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {LoginEmail} from '../../../ngrx-actions/auth.actions';
import {IEmailPasswordCredentials} from '../../../models/IEmailPasswordCredentials';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

    private returnUrl: string;
    constructor(private store: Store<state.State>,
                private route: ActivatedRoute,
                private router: Router) {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.store.select(state.getAuthLoggedIn).subscribe(auth => {
            if (auth) {
                this.router.navigate([this.returnUrl]);
            }
        });
    }

    onSubmit(cred: IEmailPasswordCredentials) {
        this.store.dispatch(new LoginEmail(cred));
    }

    ngOnInit() {
    }

}
