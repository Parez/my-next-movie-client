import { NgModule } from '@angular/core';
import { ActorPageComponent } from './actor-page/actor-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MovieModule} from '../movie/movie.module';

@NgModule({
  imports: [
    SharedModule,
    MovieModule
  ],
  declarations: [ActorPageComponent]
})
export class ActorModule { }
