import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {Observable} from 'rxjs/Observable';
import {IMovie} from '../../../models/IMovie';
import {IMovieBase} from '../../../models/IMovieBase';
import * as actorActions from '../../../ngrx-actions/actor.actions';
import {AMOUNT_PER_PAGE} from '../../../ngrx-reducers/movies-list.reducer';
import {ListDomain} from '../../../ngrx-actions/movies-list.actions';

@Component({
    selector: 'app-actor-page',
    templateUrl: './actor-page.component.html',
    styleUrls: ['./actor-page.component.scss']
})
export class ActorPageComponent implements OnInit {

    public foundMovies$: Observable<IMovie[]>;
    public actorName: string;
    public listDomain: ListDomain = ListDomain.ACTOR;
    constructor(private store: Store<state.State>,
                private route: ActivatedRoute) {

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.actorName = params.get('name');
            this.store.dispatch(new actorActions.Search({
                field: 'Actors',
                search: this.actorName,
                limit: AMOUNT_PER_PAGE
            }));
        });

        this.foundMovies$ = store.select(state.getActorResults);
    }

    ngOnInit() {
    }

    onScroll() {
        this.store.dispatch(new actorActions.SearchMore({limit: AMOUNT_PER_PAGE}));
    }
}
