import { Component, OnInit, ChangeDetectionStrategy, Input, ElementRef } from '@angular/core';
import {IMovie} from '../../../models/IMovie';
import {Router} from '@angular/router';

@Component({
  selector: 'app-movie-info-embedded',
  templateUrl: './movie-info-embedded.component.html',
  styleUrls: ['./movie-info-embedded.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieInfoEmbeddedComponent implements OnInit {

  @Input() set height(val: number) {
    console.log(val);
    this.elRef.nativeElement.style.height = val.toString() + 'px';
  }

  @Input() set movie(val: IMovie) {
    if (val) {
      this._movie = val;
      this.elRef.nativeElement.style['opacity'] = 0.9;
    }
  }
  get movie(): IMovie {
    return this._movie;
  }
  private _movie: IMovie;

  constructor(private elRef: ElementRef, private router: Router) {
    this.elRef.nativeElement.style['opacity'] = 0;
  }

  ngOnInit() {
  }

  navigateToActor(name: string): void {
    this.router.navigate(['actor', name]);
  }

  navigateToGenre(name: string): void {
    this.router.navigate(['genre', name]);
  }

  navigateToMovie(): void {
    this.router.navigate(['/movie', this.movie.imdbID]);
  }

}
