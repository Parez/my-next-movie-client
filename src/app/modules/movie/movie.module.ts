import { NgModule } from '@angular/core';
import { MovieComponent } from './movie.component';
import {SharedModule} from '../../shared/shared.module';
import {RatingModule} from 'ngx-rating';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { MovieItemListComponent } from './movie-item-list/movie-item-list.component';
import { MoviePageComponent } from './movie-page/movie-page.component';
import { MovieSearchResultsComponent } from './movie-search-results/movie-search-results.component';
import { MovieTrailerComponent } from './movie-trailer/movie-trailer.component';
import { CollectionButtonComponent } from './collection-button/collection-button.component';
import { MovieInfoEmbeddedComponent } from './movie-info-embedded/movie-info-embedded.component';

@NgModule({
  imports: [
    SharedModule,
    RatingModule
  ],
  exports: [
    MovieComponent,
    MovieItemListComponent,
    MovieSearchResultsComponent,
    MovieItemComponent,
  ],
  declarations: [
    MovieComponent,
    MovieItemComponent,
    MovieItemListComponent,
    MoviePageComponent,
    MovieSearchResultsComponent,
    MovieTrailerComponent,
    CollectionButtonComponent,
    MovieInfoEmbeddedComponent,
  ]
})
export class MovieModule { }
