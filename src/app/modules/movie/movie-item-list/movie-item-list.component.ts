import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {IMovie} from '../../../models/IMovie';

@Component({
  selector: 'app-movie-item-list',
  templateUrl: './movie-item-list.component.html',
  styleUrls: ['./movie-item-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieItemListComponent implements OnInit {

  @Input() movies: IMovie[] = [];

  constructor() { }

  ngOnInit() {
  }

  trackMovies(index, movie) {
      return movie.imdbID;
  }

}
