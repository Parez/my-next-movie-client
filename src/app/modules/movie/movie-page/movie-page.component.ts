import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {IMovie} from '../../../models/IMovie';
import {Observable} from 'rxjs/Observable';
import * as movieActions from '../../../ngrx-actions/search.actions';
import * as state from '../../../ngrx-reducers';
import {Store} from '@ngrx/store';

@Component({
    selector: 'app-movie-page',
    templateUrl: './movie-page.component.html',
    styleUrls: ['./movie-page.component.scss']
})
export class MoviePageComponent implements OnInit {

    loading$: Observable<boolean>;
    loaded$: Observable<boolean>;
    movie$: Observable<IMovie>;
    constructor(private route: ActivatedRoute, private store: Store<state.State>) {
        this.route.paramMap.subscribe((params: ParamMap) => {
            const imdbID = params.get('id');
            this.store.dispatch(new movieActions.Load({imdbID}));
        });

        this.loading$ = store.select(state.getMovieLoading);
        this.loaded$ = store.select(state.getMovieLoaded);
        this.movie$ = store.select(state.getMovie);
    }

    ngOnInit() {
    }

}
