import {Component, Input, OnInit, OnDestroy} from '@angular/core';

const MIN_WIDTH: number = 280;
const MIN_HEIGHT: number = 180;

@Component({
  selector: 'app-movie-trailer',
  templateUrl: './movie-trailer.component.html',
  styleUrls: ['./movie-trailer.component.scss']
})
export class MovieTrailerComponent implements OnInit, OnDestroy {

  @Input() set videoId(val: string) {
    this._videoId = val;
    console.log(val);
    if (this.player) {
      this.player.loadVideoById({'videoId': val});
      this.player.stopVideo();
    }
  }
  get videoId(): string {
    return this._videoId;
  }
  private _videoId: string;
  @Input() width = 460;
  @Input() height = 280;

  private player;
  private ytEvent;

  constructor() { }

  ngOnInit() {
    window.addEventListener('resize', () => this.onResize());
  }

  ngOnDestroy(): void {
    window.removeEventListener('resize', () => this.onResize());
  }

  onResize() {
    console.log('resize');
    const tgWidth = Math.max(window.innerWidth / 3, MIN_WIDTH);
    const tgHeight = Math.max(tgWidth / 1.5, MIN_HEIGHT);
    if (this.player) {
      this.player.setSize(tgWidth, tgHeight);
    }
  }

  onReady(player): void {
    this.player = player;
    this.player.stopVideo();
    this.onResize();
  }

  onChange(event): void {
    console.log(event);
  }

}
