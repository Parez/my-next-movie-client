import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {IMovie} from '../../../models/IMovie';
import {ListDomain} from '../../../ngrx-actions/movies-list.actions';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as state from '../../../ngrx-reducers/';

@Component({
  selector: 'app-movie-search-results',
  templateUrl: './movie-search-results.component.html',
  styleUrls: ['./movie-search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieSearchResultsComponent implements OnInit {
  @Input() movieResults: IMovie[] = [];
  @Input() listDomain: ListDomain = ListDomain.SEARCH;
  @Output() scroll: EventEmitter<void> = new EventEmitter();

  constructor(private store: Store<state.State>) {

  }

  ngOnInit() {
  }

  onScroll() {
    console.log('Scrolled');
    this.scroll.emit();
  }

}
