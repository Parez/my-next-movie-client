import {ChangeDetectorRef, Component, Input} from '@angular/core';
import {IMovie} from '../../models/IMovie';
import * as collectionActions from '../../ngrx-actions/collection.actions';
import {Store} from '@ngrx/store';
import * as state from '../../ngrx-reducers';
import {MoviesService} from '../../services/movies.service';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import { timer } from 'rxjs/observable/timer';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/publishReplay';

@Component({
    selector: 'app-movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.scss']
})
export class MovieComponent {

    @Input()
    set movie(val: IMovie) {
        if (!val) return;
        this._movie = val;
        timer(0, 2000).switchMapTo(this.moviesService.getMovieSimilarities(this._movie.imdbID))
          .do(console.log)
          .do(movies => this.similar = movies)
          .takeWhile(() => this.similar.length === 0)
          .subscribe();
      this.trailer$ = this.moviesService.getMovieTrailer(val.imdbID);
    }
    get movie(): IMovie {
        return this._movie;
    }
    _movie: IMovie = null;
    poster$: Observable<string>;
    trailer$: Observable<string>;

    similar: IMovie[] = [];

    constructor(private store: Store<state.State>, private moviesService: MoviesService, private router: Router,
                private ref: ChangeDetectorRef) {
    }

    navigateToActor(name: string): void {
        this.router.navigate(['actor', name]);
    }

    navigateToGenre(name: string): void {
        this.router.navigate(['genre', name]);
    }

    onAddToCollection() {
        this.store.dispatch(new collectionActions.Add({movie: this.movie}));
    }

    onRemoveFromCollection() {
        this.store.dispatch(new collectionActions.Remove({movie: this.movie}));
    }

    onLike(): void {

    }

    onDislike(): void {

    }

}
