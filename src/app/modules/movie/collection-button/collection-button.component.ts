import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {IMovie} from '../../../models/IMovie';
import {Subscription} from 'rxjs/Subscription';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-collection-button',
  templateUrl: './collection-button.component.html',
  styleUrls: ['./collection-button.component.scss']
})
export class CollectionButtonComponent implements OnInit {
  @Output() add: EventEmitter<void> = new EventEmitter();
  @Output() remove: EventEmitter<void> = new EventEmitter();

  @Input()
  set movie(val: IMovie) {
    if (!val) return;

    this._movie = val;
  }
  get movie(): IMovie {
    return this._movie;
  }
  _movie: IMovie;

  inCollection = false;
  inCollection$: Observable<boolean>;

  isAuthenticated$: Observable<boolean>;

  constructor(private store: Store<state.State>) {
      this.isAuthenticated$ = this.store.select(state.getAuthLoggedIn);
  }

  ngOnInit() {
      this.inCollection$ = this.store.select(state.getCollectionMovieIds)
          .map(ids => ids.indexOf(this._movie.imdbID) >= 0)
          .pipe(distinctUntilChanged());

      this.inCollection$.subscribe(inside => {
          this.inCollection = inside;
      });
  }

  onClick() {
      if (this.inCollection) {
          this.remove.emit();
      } else {
          this.add.emit();
      }
  }

}
