import {Component, OnInit, Input, ChangeDetectionStrategy, ElementRef, ViewChild, ChangeDetectorRef} from '@angular/core';
import {IMovie} from '../../../models/IMovie';
import {Router} from '@angular/router';
import * as collectionActions from '../../../ngrx-actions/collection.actions';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {PopupService} from '../../../services/popup.service';
import {TouchService} from '../../../services/touch.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieItemComponent implements OnInit {

  @ViewChild('container', {read: ElementRef}) container;

  @Input()
  set movie(val: IMovie) {
    if (!val) return;
    this._movie = val;
  }
  get movie(): IMovie {
    return this._movie;
  }
  _movie: IMovie = null;

  showInfo: boolean = false;
  curHeight: number = 0;
  touchSub: Subscription;

  constructor(private router: Router,
              private store: Store<state.State>, private touchService: TouchService, private ref: ChangeDetectorRef) {

  }

  ngOnInit() {
  }

  onAddToCollection() {
      this.store.dispatch(new collectionActions.Add({movie: this.movie}));
  }

  onRemoveFromCollection() {
      this.store.dispatch(new collectionActions.Remove({movie: this.movie}));
  }

  navigateToMovie() {
    console.log('Navigate');
    this.router.navigate(['/movie', this.movie.imdbID]);
  }

  onInfoShow(event: MouseEvent) {
    this.curHeight = this.container.nativeElement.offsetHeight;

    this.touchSub = this.touchService.touch$.subscribe(pos => {
      const rect = this.container.nativeElement.getBoundingClientRect();
      const left = rect.left + window.scrollX;
      const right = rect.right + window.scrollX;
      const top = rect.top + window.scrollY;
      const bottom = rect.bottom + window.scrollY;
      console.log(pos.x, left + window.scrollX, right);
      console.log(pos.y, top, bottom);
      if (pos.x < left || pos.x > right) {
        this.onInfoHide();
      }
      if (pos.y < top || pos.y > bottom) {
        this.onInfoHide();
      }
    });

    console.log(this.curHeight);

    this.showInfo = true;
  }

  onInfoHide() {
    console.log('Hide');
    // this.popupService.mouseOnInfo(false);
    this.showInfo = false;
    this.touchSub.unsubscribe();
    this.ref.markForCheck();
  }
}
