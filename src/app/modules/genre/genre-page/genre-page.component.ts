import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {Observable} from 'rxjs/Observable';
import {IMovie} from '../../../models/IMovie';
import * as genreActions from '../../../ngrx-actions/genre.actions';
import {AMOUNT_PER_PAGE} from '../../../ngrx-reducers/movies-list.reducer';
import {ListDomain} from '../../../ngrx-actions/movies-list.actions';

@Component({
  selector: 'app-genre-page',
  templateUrl: './genre-page.component.html',
  styleUrls: ['./genre-page.component.scss']
})
export class GenrePageComponent implements OnInit {

    public foundMovies$: Observable<IMovie[]>;
    public genreName: string;
    public listDomain: ListDomain = ListDomain.GENRE;
    constructor(private store: Store<state.State>,
                private route: ActivatedRoute) {

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.genreName = params.get('name');
            console.log('Genres', this.genreName);
            this.store.dispatch(new genreActions.Search({
                field: 'Genres',
                search: this.genreName,
                limit: AMOUNT_PER_PAGE
            }));
        });

        this.foundMovies$ = store.select(state.getGenreResults);
    }

    ngOnInit() {
    }

    onScroll() {
        this.store.dispatch(new genreActions.SearchMore({limit: AMOUNT_PER_PAGE}));
    }
}
