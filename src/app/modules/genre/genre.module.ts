import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenrePageComponent } from './genre-page/genre-page.component';
import {MovieModule} from '../movie/movie.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      SharedModule,
      MovieModule
  ],
  declarations: [GenrePageComponent]
})
export class GenreModule { }
