import { Component, OnInit } from '@angular/core';
import {EVERY_REACTIONS, MIN_REACTIONS, MoviesService} from '../../../services/movies.service';
import {Observable} from 'rxjs/Observable';
import { timer } from 'rxjs/observable/timer';
import 'rxjs/add/operator/takeUntil';
import {IRecommendation} from '../../../models/IRecommendation';
import {Store} from '@ngrx/store';
import * as state from '../../../ngrx-reducers';
import {getReactionsCounterCount} from '../../../ngrx-reducers/index';
import * as recommendationsActions from '../../../ngrx-actions/recommendations.actions';

@Component({
  selector: 'app-recommendations-page',
  templateUrl: './recommendations-page.component.html',
  styleUrls: ['./recommendations-page.component.scss']
})
export class RecommendationsPageComponent implements OnInit {

  numRated: number = 0;
  recommendations$: Observable<IRecommendation[]>;
  constructor(private store: Store<state.State>) {
    this.recommendations$ = this.store.select(state.getRecommendations);
    this.store.select(getReactionsCounterCount).subscribe(count => {
      this.numRated = count;
    });
  }

  isReady(): boolean {
    return (this.numRated >= MIN_REACTIONS && this.numRated % EVERY_REACTIONS === 0);
  }

  ngOnInit() {
  }

}
