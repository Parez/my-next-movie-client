import {Component, Input, OnInit} from '@angular/core';
import {IRecommendation} from '../../../models/IRecommendation';

@Component({
  selector: 'app-recommended-movie-list',
  templateUrl: './recommended-movie-list.component.html',
  styleUrls: ['./recommended-movie-list.component.scss']
})
export class RecommendedMovieListComponent implements OnInit {

  @Input() recommendations: IRecommendation[];
  constructor() { }

  ngOnInit() {
  }

}
