import { NgModule } from '@angular/core';
import { RecommendationsPageComponent } from './recommendations-page/recommendations-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MovieModule} from '../movie/movie.module';
import { RecommendedMovieComponent } from './recommended-movie/recommended-movie.component';
import { RecommendedMovieListComponent } from './recommended-movie-list/recommended-movie-list.component';

@NgModule({
  imports: [
    SharedModule,
    MovieModule
  ],
  declarations: [RecommendationsPageComponent, RecommendedMovieComponent, RecommendedMovieListComponent]
})
export class RecommendationsModule { }
