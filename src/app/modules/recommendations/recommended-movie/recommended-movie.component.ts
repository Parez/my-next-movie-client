import {Component, Input, OnInit} from '@angular/core';
import {IRecommendation} from '../../../models/IRecommendation';

@Component({
  selector: 'app-recommended-movie',
  templateUrl: './recommended-movie.component.html',
  styleUrls: ['./recommended-movie.component.scss'],
})
export class RecommendedMovieComponent implements OnInit {

  @Input() recommendation: IRecommendation;
  constructor() { }

  ngOnInit() {
  }

}
