/**
 * Created by baunov on 07/11/2017.
 */
import { environment } from '../environments/environment';

export const BASE_URL = environment.apiUrl;

export const searchMovies = `${BASE_URL}/movies/search`;
export const searchMoviesByTitle = `${BASE_URL}/movies/name`;
export const getMovieByImdbId = `${BASE_URL}/movies/id`;
export const likeMovie = `${BASE_URL}/movies/like`;
export const dislikeMovie = `${BASE_URL}/movies/dislike`;
export const getMovieReaction = `${BASE_URL}/movies/reaction`;
export const getRecommendations = `${BASE_URL}/movies/recommendations`;

export const getMoviePoster = `${BASE_URL}/movies/poster`;
export const getMovieTrailer = `${BASE_URL}/movies/trailer`;

export const movieLikes = `${BASE_URL}/movies/likes`;
export const movieDislikes = `${BASE_URL}/movies/dislikes`;
export const numReactions = `${BASE_URL}/movies/numReactions`;

// Collection
export const addMovieToCollection = `${BASE_URL}/collections/add`;
export const removeMovieFromCollection = `${BASE_URL}/collections/remove`;
export const getCollectionMovies = `${BASE_URL}/collections`;
export const getCollectionIds = `${BASE_URL}/collections/ids`;

// Similarities
export const getMovieSimilarities = `${BASE_URL}/movies/similarities`;

export const register = `${BASE_URL}/users/auth/register`;
export const login = `${BASE_URL}/users/auth/login`;
export const authFacebook = `${BASE_URL}/users/auth/facebook`;
export const getCurrentUser = `${BASE_URL}/users/me`;
export const checkAuth = `${BASE_URL}/users/auth/check`;
