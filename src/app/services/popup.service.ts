import { Injectable } from '@angular/core';
import { IPopupData } from '../models/IPopupData';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PopupService {

  private selfOn: boolean = false;
  private infoOn: boolean = false;
  private popupSubj: Subject<IPopupData> = new Subject();
  public popup$: Observable<IPopupData> = this.popupSubj.asObservable();
  constructor() { }

  showPopup(data: IPopupData) {
    this.infoOn = true;
    this.popupSubj.next(data);
  }

  mouseOnInfo(isOn: boolean = false) {
    this.infoOn = isOn;
    this.hidePopup();
  }

  mouseOnPanel(isOn: boolean = false) {
    this.selfOn = isOn;
    this.hidePopup();
  }

  private hidePopup() {
    if (!this.selfOn && !this.infoOn) {
      setTimeout(() => {
        if (!this.selfOn && !this.infoOn) {
          this.popupSubj.next(null);
        }
      }, 100);
    }
  }

  forceHide() {
    this.popupSubj.next(null);
  }
}
