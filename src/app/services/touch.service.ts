import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TouchService {

  private touchSubj: Subject<{x: number, y: number}> = new Subject();
  touch$: Observable<{x: number, y: number}> = this.touchSubj.asObservable();
  constructor() {
    window.addEventListener('touchstart', (event) => {
      const touches: TouchList = event.targetTouches;
      console.log({x: touches.item(0).pageX, y: touches.item(0).pageY});
      this.touchSubj.next({x: touches.item(0).pageX, y: touches.item(0).pageY});
    });
  }

}
