import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/mergeMap';
import { of } from 'rxjs/observable/of';
import { HttpParams } from '@angular/common/http';
import { IMovie } from '../models/IMovie';
import {
  getMovieByImdbId, getMovieSimilarities, likeMovie, searchMovies, searchMoviesByTitle, getRecommendations,
  dislikeMovie, getMovieReaction, movieLikes, movieDislikes, numReactions, getMoviePoster, getMovieTrailer
} from '../api';
import { IMovieBase } from '../models/IMovieBase';
import { IMovieSearchOptions } from '../models/IMovieSearchOptions';
import {IRecommendation} from '../models/IRecommendation';
import {ResponseContentType} from '@angular/http';

export const MIN_REACTIONS = 10;
export const EVERY_REACTIONS = 5;

@Injectable()
export class MoviesService {

    constructor(private http: HttpClient) {

    }

    numReactions(): Observable<number> {
      return this.http.get(`${numReactions}`).map(res => {
        return res['data'];
      });
    }

    numReactionsLeft(curReactions: number): number {
      if (curReactions < MIN_REACTIONS) {
        return MIN_REACTIONS - curReactions;
      } else {
        return MIN_REACTIONS + EVERY_REACTIONS * Math.floor((curReactions - MIN_REACTIONS) / EVERY_REACTIONS + 1)  - curReactions;
      }
    }

    getMovieTrailer(movieId: number): Observable<string> {
      return this.http.get(`${getMovieTrailer}/${movieId}`).map(res => {
        return res['data'];
      });
    }

    getMoviesLikes(): Observable<IMovie[]> {
      return this.http.get(`${movieLikes}`).map(res => {
        return res['data'];
      });
    }

    getMoviesDislikes(): Observable<IMovie[]> {
      return this.http.get(`${movieDislikes}`).map(res => {
        return res['data'];
      });
    }

    likeMovie(movieId: number): Observable<number> {
        return this.http.post(`${likeMovie}/${movieId}`, null).map(res => {
            console.log(res);
            return res['data'];
        });
    }

    dislikeMovie(movieId: number): Observable<number> {
        return this.http.post(`${dislikeMovie}/${movieId}`, null).map(res => {
            return res['data'];
        });
    }

    getUserMovieReaction(movieId: number): Observable<number> {
        return this.http.get(`${getMovieReaction}/${movieId}`).map(res => {
            return res['data'] ? res['data'] : null;
        });
    }

    searchMoviesByTitle(title: string): Observable<IMovieBase[]> {
        return this.http.get(`${searchMoviesByTitle}/${title.toLowerCase()}`).map(res => {
            return res['data'];
        });
    }

    getRecommendations(): Observable<IRecommendation[]> {
        return this.http.get(`${getRecommendations}`).map(res => {
            return res['data'];
        });
    }

    // returns array of {title, imdbId}
    searchMovies(options: IMovieSearchOptions): Observable<IMovie[]> {
        const params = new HttpParams()
            .set('skip', String(options.skip | 0))
            .set('limit', String(options.limit | 10))
            .set('sortOn', options.sortOn)
            .set('sortOrder', String(options.sortOrder | -1))
            .set('field', options.field || '');

        return this.http.get(`${searchMovies}/${options.search.toLowerCase()}`, { params }).map(res => {
            console.log(res);
            return res['data'];
        });
    }

    getMovieDataByImdbId(id: number): Observable<IMovie> {
        return this.http.get(`${getMovieByImdbId}/${id}`).map( res => {
            return res['data'];
        }).do(console.log);
    }

    getMoviesDataByImdbIds(ids: number[]): Observable<IMovie[]> {
        return of(...ids).flatMap( (id) => {
            return this.getMovieDataByImdbId(id);
        }).scan( (acc, movie) => {
            return acc.concat(movie);
        }, []);
    }

    getMovieSimilarities(id: number): Observable<IMovie[]> {
        return this.http.get(`${getMovieSimilarities}/${id}`).map( res => {
            return res['data'];
        });
    }

    getMoviePoster(id: number): Observable<string> {
      return this.http.get(`${getMoviePoster}/${id}`, { responseType: 'blob' }).map( res => {
        return new Blob([res], {type: 'image/jpeg'});
      }).switchMap(blob => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        return Observable.fromEvent(reader, 'load').map(() => reader.result);
      });
    }
}
