/**
 * Created by baunov on 07/11/2017.
 */
import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { InitParams, FacebookService, LoginResponse } from 'ngx-facebook';
import { HttpClient } from '@angular/common/http';
import {authFacebook, checkAuth, getCurrentUser, login, register} from '../api';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMapTo';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/observable/throw';
import { HttpResponse } from '@angular/common/http';
import {IEmailPasswordCredentials} from '../models/IEmailPasswordCredentials';
import {IRegisterCredentials} from '../models/IRegisterCredentials';
import {IUser} from '../models/IUser';

@Injectable()
export class AuthService {

    constructor(private fb: FacebookService, private http: HttpClient) {
        const initParams: InitParams = {
            appId: '859586494210058',
            xfbml: true,
            version: 'v2.9'
        };

        fb.init(initParams);
    }

    public static getToken() {
        return localStorage.getItem('id_token');
    }

    public static isAuthenticated(): boolean {
        // get the token
        const token = this.getToken();
        // return a boolean reflecting
        // whether or not the token is expired
        return tokenNotExpired(null, token);
    }

    public static getUser(): IUser {
        if (this.isAuthenticated()) {
            return JSON.parse(localStorage.getItem('user')) as IUser;
        }
        return null;
    }

    public loginWithFacebook(): Observable<any> {
        return Observable.defer(() =>
            Observable.defer(() => this.fb.login()).flatMap((userData) => {
                console.log(userData.authResponse.accessToken);
                return this.http.post(authFacebook, {access_token: userData.authResponse.accessToken}, { observe: 'response' });
            }).do( (response: HttpResponse<any>) => {
                const token = response.headers.get('x-auth-token');
                this.saveToken(token);
            })
        );
    }

    getFacebookProfile(): Observable<any> {
        return Observable.defer(() =>
            Observable.defer(() => this.fb.getLoginStatus())
                .filter((state) => state.status === 'connected')
                .switchMapTo(Observable.fromPromise(this.fb.api('/me')))
        );
    }

    emailPasswordLogin(credentials: IEmailPasswordCredentials): Observable<IUser> {
        return this.http.post(login, credentials, { observe: 'response' }).do( (response: HttpResponse<any>) => {
            console.log(response.body);
            const token = response.headers.get('x-auth-token');
            this.saveToken(token);
            this.saveUser(response.body);
        }).map(res => res.body);
    }

    register(credentials: IRegisterCredentials): Observable<IUser> {
        return this.http.post(register, credentials, { observe: 'response' }).do( (response: HttpResponse<any>) => {
            console.log(response.body);
            const token = response.headers.get('x-auth-token');
            this.saveToken(token);
            this.saveUser(response.body);
        }).map(res => res.body);
    }

    private saveToken(token: string): void {
        if (token) {
            localStorage.setItem('id_token', token);
        }
    }

    private saveUser(user: IUser): void {
        if (user) {
            localStorage.setItem('user', JSON.stringify(user));
        }
    }

    checkAuth(): Observable<any> {
      return this.http.get(checkAuth);
    }

    logout() {
        localStorage.removeItem('id_token');
        localStorage.removeItem('user');
        localStorage.removeItem('auth');
    }

    getCurrentUser(): Observable<IUser> {
        if (AuthService.isAuthenticated()) {
            return this.http.get(getCurrentUser).map(user => user as IUser);
        }
        return Observable.throw('User not authenticated');
    }
}
