import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import {
    addMovieToCollection, getCollectionMovies, getMovieByImdbId, removeMovieFromCollection, searchMovies,
    searchMoviesByTitle
} from '../api';
import {IMovieLoadOptions} from '../models/IMovieLoadOptions';

@Injectable()
export class CollectionService {

  constructor(private http: HttpClient) {

  }

  addMovie(imdbId: number): Observable<any> {
      return this.http.post(`${addMovieToCollection}/${imdbId}`, null);
  }

  removeMovie(imdbId: number): Observable<any> {
    return this.http.post(`${removeMovieFromCollection}/${imdbId}`, null);
  }

  getCollection(options: IMovieLoadOptions): Observable<any> {
      const params = new HttpParams()
          .set('skip', String(options.skip | 0))
          .set('limit', String(options.limit | 10));
      return this.http.get(`${getCollectionMovies}`, { params }).map(res => {
          console.log(res);
          return res['data'];
      });
  }

}
