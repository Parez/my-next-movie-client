/**
 * Created by baunov on 14/11/2017.
 */
import * as AuthActions from '../ngrx-actions/auth.actions';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import { of } from 'rxjs/observable/of';
import {IEmailPasswordCredentials} from '../models/IEmailPasswordCredentials';
import {IRegisterCredentials} from '../models/IRegisterCredentials';
import {IUser} from '../models/IUser';

@Injectable()
export class AuthEffects {


    @Effect()
    login$: Observable<CustomAction> = this.actions$
        .ofType(AuthActions.LOGIN_EMAIL)
        .map((action: AuthActions.LoginEmail) => action.payload)
        .switchMap((creds: IEmailPasswordCredentials) => {
            return this.authService.emailPasswordLogin(creds)
                .map((user: IUser) => new AuthActions.LoginEmailComplete({user}))
                .catch((err) => of(new AuthActions.LoginEmailError(err)));
        });

    @Effect()
    loadUser$: Observable<CustomAction> = this.actions$
        .ofType(AuthActions.LOAD_USER)
        .switchMap(() => {
            return this.authService.getCurrentUser()
                .map((user: IUser) => new AuthActions.LoadUserComplete({user}))
                .catch((err) => of(new AuthActions.LoadUserError(err)));
        });


    @Effect()
    register$: Observable<CustomAction> = this.actions$
        .ofType(AuthActions.REGISTER)
        .do(console.log)
        .map((action: AuthActions.Register) => action.payload)
        .switchMap((creds: IRegisterCredentials) => {
            return this.authService.register(creds)
                .map((user: IUser) => new AuthActions.RegisterComplete({user}))
                .catch((err) => of(new AuthActions.RegisterError(err)));
        });

    @Effect()
    logout$: Observable<CustomAction> = this.actions$
        .ofType(AuthActions.LOGOUT)
        .map(() => this.authService.logout())
        .mapTo(new AuthActions.LogoutComplete());

  @Effect()
  check$: Observable<CustomAction> = this.actions$
    .ofType(AuthActions.CHECK_AUTH)
    .flatMap(() => {
      return this.authService.checkAuth()
        .map(() => new AuthActions.CheckAuthComplete())
        .catch((err) => of(new AuthActions.CheckAuthError(err)));
    });

    constructor(private actions$: Actions,
                private authService: AuthService) {

    }
}
