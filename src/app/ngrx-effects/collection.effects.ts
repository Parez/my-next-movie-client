/**
 * Created by baunov on 14/11/2017.
 */
import * as collection from '../ngrx-actions/collection.actions';
import { IMovie } from '../models/IMovie';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MoviesService } from '../services/movies.service';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';

import {Store} from '@ngrx/store';
import {State} from '../ngrx-reducers/index';
import {CollectionService} from '../services/collection.service';

@Injectable()
export class CollectionEffects {


    @Effect()
    loadMovies$: Observable<CustomAction> = this.actions$
        .ofType(collection.LOAD)
        .map((action: collection.Load) => action.payload)
        .switchMap((options) => {
            return this.collectionService.getCollection(options)
                .map((movieItems: IMovie[]) => new collection.LoadComplete({movies : movieItems}))
                .catch((err) => of(new collection.LoadError(err)));
        });


    @Effect()
    loadMoreStart$: Observable<CustomAction> = this.actions$
        .ofType(collection.LOAD_MORE_START)
        .map((action: collection.LoadMoreStart) => action.payload.limit)
        .withLatestFrom(this.actions$.ofType(collection.LOAD).map((action: collection.Load) => action.payload),
                        this.store$.select(state => state.search))
        .switchMap(([limit, options, state]) => {
            const reqOptions = Object.assign({}, options, {limit}, {skip: limit * state.pageNumber});
            return this.collectionService.getCollection(reqOptions)
                .map((movieItems: IMovie[]) => new collection.LoadMoreComplete({movies : movieItems}))
                .catch((err) => of(new collection.LoadMoreError(err)));
        });



    @Effect()
    loadMore$: Observable<CustomAction> = this.actions$
        .ofType(collection.LOAD_MORE)
        .withLatestFrom(this.store$.select(state => state.search))
        .filter(([action, store]) => store.loading === false && store.allLoaded === false)
        .map(([action, store]) => action)
        .map((action: collection.LoadMore) => action)
        .map((action) => {
            if (action) {
                return new collection.LoadMoreStart({limit: action.payload.limit});
            }
        });

    @Effect()
    addMovie$: Observable<CustomAction> = this.actions$
        .ofType(collection.ADD)
        .map((action: collection.Add) => action.payload.movie)
        .switchMap((movie) => {
            return this.collectionService.addMovie(movie.imdbID).map(() => movie)
                .map((addedMovie) => new collection.AddComplete({movie: addedMovie}))
                .catch((err) => of(new collection.AddError(err)));
        });

    @Effect()
    removeMovie$: Observable<CustomAction> = this.actions$
        .ofType(collection.REMOVE)
        .map((action: collection.Remove) => action.payload.movie)
        .switchMap((movie) => {
            return this.collectionService.removeMovie(movie.imdbID).map(() => movie)
                .map((removedMovie) => new collection.RemoveComplete({movie: removedMovie}))
                .catch((err) => of(new collection.RemoveError(err)));
        });



    constructor(private actions$: Actions,
                private store$: Store<State>,
                private collectionService: CollectionService) {}
}
