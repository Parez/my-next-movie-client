/**
 * Created by baunov on 14/11/2017.
 */
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MoviesService } from '../services/movies.service';
import {Store} from '@ngrx/store';
import {State} from '../ngrx-reducers/index';
import {MoviesListEffects} from './movies-list.effects';
import {ListDomain} from '../ngrx-actions/movies-list.actions';

@Injectable()
export class ActorEffects extends MoviesListEffects {

    constructor(actions$: Actions,
                store$: Store<State>,
                moviesService: MoviesService) {
        super(actions$, store$, moviesService, ListDomain.ACTOR);
    }
}
