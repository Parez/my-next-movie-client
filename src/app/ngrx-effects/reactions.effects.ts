/**
 * Created by baunov on 14/11/2017.
 */
import * as ReactionsActions from '../ngrx-actions/reactions.actions';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { MoviesService } from '../services/movies.service';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import { of } from 'rxjs/observable/of';
import {getAuthLoggedIn, State} from '../ngrx-reducers/index';
import {Store} from '@ngrx/store';
import {IMovie} from '../models/IMovie';

@Injectable()
export class ReactionsEffects {

  @Effect()
  loadLikes$: Observable<CustomAction> = this.actions$
      .ofType(ReactionsActions.LOAD_LIKES)
      .withLatestFrom(this.store$.select(getAuthLoggedIn))
      .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
      .map((action: ReactionsActions.LoadLikes) => action)
      .flatMapTo(this.moviesService.getMoviesLikes())
      .map((likes: IMovie[]) => new ReactionsActions.LoadLikesComplete({movies: likes}))
      .catch((err) => of(new ReactionsActions.LoadLikesError(err)));

  @Effect()
  loadDislikes$: Observable<CustomAction> = this.actions$
    .ofType(ReactionsActions.LOAD_DISLIKES)
    .withLatestFrom(this.store$.select(getAuthLoggedIn))
    .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
    .map((action: ReactionsActions.LoadDislikes) => action)
    .flatMapTo(this.moviesService.getMoviesDislikes())
    .map((dislikes: IMovie[]) => new ReactionsActions.LoadDislikesComplete({movies: dislikes}))
    .catch((err) => of(new ReactionsActions.LoadDislikesError(err)));

  @Effect()
  countReactions$: Observable<CustomAction> = this.actions$
      .ofType(ReactionsActions.COUNT_REACTIONS)
      .withLatestFrom(this.store$.select(getAuthLoggedIn))
      .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
      .map((action: ReactionsActions.CountReactions) => action)
      .flatMapTo(this.moviesService.numReactions())
      .map((count: number) => new ReactionsActions.CountReactionsComplete(count))
      .catch((err) => of(new ReactionsActions.CountReactionsError(err)));

  @Effect()
  like$: Observable<CustomAction> = this.actions$
    .ofType(ReactionsActions.LIKE)
    .withLatestFrom(this.store$.select(getAuthLoggedIn))
    .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
    .map((action: ReactionsActions.Like) => action.payload)
    .flatMap((payload) => {
      return this.moviesService.likeMovie(payload.movie.imdbID)
        .map(count => ({movie: payload.movie, count}));
    })
    .map(({movie, count}) => {
      console.log(count);
      return new ReactionsActions.LikeComplete({movie, count})
    })
    .catch((err) => of(new ReactionsActions.LikeError(err)));

  @Effect()
  dislike$: Observable<CustomAction> = this.actions$
    .ofType(ReactionsActions.DISLIKE)
    .withLatestFrom(this.store$.select(getAuthLoggedIn))
    .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
    .map((action: ReactionsActions.Like) => action.payload)
    .flatMap((payload) => {
      return this.moviesService.dislikeMovie(payload.movie.imdbID)
        .map(count => ({movie: payload.movie, count}));
    })
    .map(({movie, count}) => new ReactionsActions.DislikeComplete({movie, count}))
    .catch((err) => of(new ReactionsActions.DislikeError(err)));

  @Effect()
  loadReaction$: Observable<CustomAction> = this.actions$
    .ofType(ReactionsActions.LOAD_MOVIE_REACTION)
    .withLatestFrom(this.store$.select(getAuthLoggedIn))
    .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
    .map((action: ReactionsActions.LoadMovieReaction) => action.payload)
    .flatMap((payload) => {
      return this.moviesService.getUserMovieReaction(payload.movie.imdbID)
        .map(reaction => ({movie: payload.movie, reaction}));
    })
    .map(({movie, reaction}) => new ReactionsActions.LoadMovieReactionComplete({movie, reaction}))
    .catch((err) => of(new ReactionsActions.LoadMovieReactionError(err)));

    constructor(private actions$: Actions,
                private store$: Store<State>,
                private moviesService: MoviesService) {

    }
}
