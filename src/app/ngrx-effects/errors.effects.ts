/**
 * Created by baunov on 14/11/2017.
 */
import * as AuthActions from '../ngrx-actions/auth.actions';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';

@Injectable()
export class ErrorsEffects {


    // logout once unauthorized access detected
    @Effect()
    unauthorized$: Observable<CustomAction> = this.actions$
        .filter((action: any) => action.payload)
        .map((action: CustomAction) => action.payload.status)
        .filter(status => status === 401)
        .mapTo(new AuthActions.Logout());


    constructor(private actions$: Actions,
                private authService: AuthService) {

    }
}
