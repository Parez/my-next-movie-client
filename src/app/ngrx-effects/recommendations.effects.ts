/**
 * Created by baunov on 14/11/2017.
 */
import * as RecommendationsActions from '../ngrx-actions/recommendations.actions';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { MoviesService } from '../services/movies.service';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import { of } from 'rxjs/observable/of';
import {getAuthLoggedIn, State} from '../ngrx-reducers/index';
import {Store} from '@ngrx/store';
import {IRecommendation} from '../models/IRecommendation';

@Injectable()
export class RecommendationsEffects {

  @Effect()
  loadLikes$: Observable<CustomAction> = this.actions$
      .ofType(RecommendationsActions.LOAD)
      .withLatestFrom(this.store$.select(getAuthLoggedIn))
      .filter(([action, loggedIn]) => loggedIn).map(([action, loggedIn]) => action)
      .map((action: RecommendationsActions.Load) => action)
      .flatMapTo(this.moviesService.getRecommendations())
      .map((recs: IRecommendation[]) => new RecommendationsActions.LoadComplete({recs}))
      .catch((err) => of(new RecommendationsActions.LoadError(err)));

    constructor(private actions$: Actions,
                private store$: Store<State>,
                private moviesService: MoviesService) {

    }
}
