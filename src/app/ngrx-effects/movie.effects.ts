/**
 * Created by baunov on 14/11/2017.
 */
import * as MovieActions from '../ngrx-actions/search.actions';
import { IMovie } from '../models/IMovie';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { MoviesService } from '../services/movies.service';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import { of } from 'rxjs/observable/of';
import {Store} from '@ngrx/store';
import {State} from '../ngrx-reducers/index';
import {Search, SearchMore} from '../ngrx-actions/search.actions';

@Injectable()
export class MovieEffects {


    @Effect()
    searchMovies$: Observable<CustomAction> = this.actions$
        .ofType(MovieActions.LOAD)
        .map((action: MovieActions.Load) => action.payload.imdbID)
        .do(console.log)
        .withLatestFrom(this.store$.select((store) => store.movies))
        .switchMap(([id, storeMovies]) => {
            console.log('Load Movie', storeMovies, id);
            if ((storeMovies.ids as number[]).indexOf(+id) > -1) {
                return of(storeMovies.entities[id]);
            }
            return this.moviesService.getMovieDataByImdbId(id);
        })
        .map((movieItem: IMovie) => new MovieActions.LoadComplete({movie : movieItem}))
        .catch((err) => of(new MovieActions.LoadError(err)));

    constructor(private actions$: Actions,
                private store$: Store<State>,
                private moviesService: MoviesService) {

    }
}
