/**
 * Created by baunov on 14/11/2017.
 */
import * as listActions from '../ngrx-actions/movies-list.actions';
import { IMovie } from '../models/IMovie';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MoviesService } from '../services/movies.service';
import {CustomAction} from '../ngrx-actions/CustomAction';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';

import {Store} from '@ngrx/store';
import {State} from '../ngrx-reducers/index';
import {CollectionService} from '../services/collection.service';
import {getActionType, ListDomain} from '../ngrx-actions/movies-list.actions';

export class MoviesListEffects {

    @Effect()
    loadMovies$: Observable<CustomAction> = this.actions$
        .ofType(getActionType(listActions.LOAD, this.domain))
        .map((action: listActions.Load) => action.payload)
        .switchMap((options) => {
            return this.moviesService.searchMovies(options);
        })
        .map((movieItems: IMovie[]) => new listActions.LoadComplete({movies : movieItems}, this.domain))
        .catch((err) => of(new listActions.LoadError(err, this.domain)));


    @Effect()
    loadMoreStart$: Observable<CustomAction> = this.actions$
        .ofType(getActionType(listActions.LOAD_MORE_START, this.domain))
        .map((action: listActions.LoadMoreStart) => action.payload.limit)
        .withLatestFrom(
            this.actions$.ofType(getActionType(listActions.LOAD, this.domain)).map((action: listActions.Load) => action.payload),
            this.store$.select(state => state[this.domain.toLowerCase()]))
        .switchMap(([limit, options, state]) => {
            const reqOptions = Object.assign({}, options, {limit}, {skip: limit * state.pageNumber});
            return this.moviesService.searchMovies(reqOptions);
        })
        .map((movieItems: IMovie[]) => new listActions.LoadMoreComplete({movies : movieItems}, this.domain))
        .catch((err) => of(new listActions.LoadMoreError(err, this.domain)));


    @Effect()
    loadMore$: Observable<CustomAction> = this.actions$
        .ofType(getActionType(listActions.LOAD_MORE, this.domain))
        .withLatestFrom(this.store$.select(state => state[this.domain.toLowerCase()]))
        .filter(([action, store]) => store.loading === false && store.allLoaded === false)
        .map(([action, store]) => action)
        .map((action: listActions.LoadMore) => action)
        .map((action) => {
            if (action) {
                return new listActions.LoadMoreStart({limit: action.payload.limit}, this.domain);
            }
        });


    constructor(private actions$: Actions,
                private store$: Store<State>,
                private moviesService: MoviesService,
                private domain: ListDomain) {}
}
