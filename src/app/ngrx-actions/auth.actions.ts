/**
 * Created by baunov on 14/11/2017.
 */
import {CustomAction} from './CustomAction';
import {IEmailPasswordCredentials} from '../models/IEmailPasswordCredentials';
import {IRegisterCredentials} from '../models/IRegisterCredentials';

export const CHECK_AUTH = '[Auth] Check Auth';
export const CHECK_AUTH_COMPLETE = '[Auth] Check Auth Complete';
export const CHECK_AUTH_ERROR = '[Auth] Check Auth Error';

export const LOGIN_EMAIL = '[Auth] Login';
export const LOGIN_EMAIL_COMPLETE = '[Auth] Login Complete';
export const LOGIN_EMAIL_ERROR = '[Auth] Login Error';

export const LOAD_USER = '[Auth] Load User';
export const LOAD_USER_COMPLETE = '[Auth] Load User Complete';
export const LOAD_USER_ERROR = '[Auth] Load User Error';

export const LOGOUT = '[Auth] Logout';
export const LOGOUT_COMPLETE = '[Auth] Logout Complete';

export const REGISTER = '[Auth] Register';
export const REGISTER_COMPLETE = '[Auth] Register Complete';
export const REGISTER_ERROR = '[Auth] Register Error';

export class CheckAuth implements CustomAction {
  readonly type = CHECK_AUTH;
  constructor(public payload = null) {}
}

export class CheckAuthComplete implements CustomAction {
  readonly type = CHECK_AUTH_COMPLETE;
  constructor(public payload = null) {}
}

export class CheckAuthError implements CustomAction {
  readonly type = CHECK_AUTH_ERROR;
  constructor(public payload: { error: string }) {}
}

export class LoginEmail implements CustomAction {
    readonly type = LOGIN_EMAIL;
    constructor(public payload: IEmailPasswordCredentials) {}
}

export class LoginEmailComplete implements CustomAction {
    readonly type = LOGIN_EMAIL_COMPLETE;
    constructor(public payload: { user: any }) {}
}

export class LoginEmailError implements CustomAction {
    readonly type = LOGIN_EMAIL_ERROR;
    constructor(public payload: { error: string }) {}
}

export class LoadUser implements CustomAction {
    readonly type = LOAD_USER;
    constructor() {}
}

export class LoadUserComplete implements CustomAction {
    readonly type = LOAD_USER_COMPLETE;
    constructor(public payload: { user: any }) {}
}

export class LoadUserError implements CustomAction {
    readonly type = LOAD_USER_ERROR;
    constructor(public payload: { error: string }) {}
}

export class Logout implements CustomAction {
    readonly type = LOGOUT;
    constructor() {}
}

export class LogoutComplete implements CustomAction {
    readonly type = LOGOUT_COMPLETE;
    constructor() {}
}

export class Register implements CustomAction {
    readonly type = REGISTER;
    constructor(public payload: IRegisterCredentials) {}
}

export class RegisterComplete implements CustomAction {
    readonly type = REGISTER_COMPLETE;
    constructor(public payload: { user: any }) {}
}

export class RegisterError implements CustomAction {
    readonly type = REGISTER_ERROR;
    constructor(public payload: { error: string }) {}
}

export type All =
    CheckAuth |
    CheckAuthComplete |
    CheckAuthError |
    LoadUser |
    LoadUserComplete |
    LoadUserError |
    LoginEmail |
    LoginEmailComplete |
    LoginEmailError |
    Logout |
    LogoutComplete |
    Register |
    RegisterComplete |
    RegisterError;
