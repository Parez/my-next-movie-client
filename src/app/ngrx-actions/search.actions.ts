/**
 * Created by baunov on 14/11/2017.
 */
import { IMovie } from '../models/IMovie';
import {CustomAction} from './CustomAction';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import * as listActions from './movies-list.actions';
import {ListDomain} from './movies-list.actions';
export const SELECT = '[Movie] Select';
export const LOAD = '[Movie] Load';
export const LOAD_COMPLETE = '[Movie] Load Complete';
export const LOAD_ERROR = '[Movie] Load Error';

export const SEARCH = listActions.getActionType(listActions.LOAD, ListDomain.SEARCH);
export const SEARCH_COMPLETE = listActions.getActionType(listActions.LOAD_COMPLETE, ListDomain.SEARCH);
export const SEARCH_ERROR = listActions.getActionType(listActions.LOAD_ERROR, ListDomain.SEARCH);
export const SEARCH_MORE = listActions.getActionType(listActions.LOAD_MORE, ListDomain.SEARCH);
export const SEARCH_MORE_START = listActions.getActionType(listActions.LOAD_MORE_START, ListDomain.SEARCH);
export const SEARCH_MORE_COMPLETE = listActions.getActionType(listActions.LOAD_MORE_COMPLETE, ListDomain.SEARCH);
export const SEARCH_MORE_ERROR = listActions.getActionType(listActions.LOAD_MORE_ERROR, ListDomain.SEARCH);

export class Search extends listActions.Load {
    constructor(public payload: IMovieSearchOptions) {
        super(payload, ListDomain.SEARCH);
    }
}

export class SearchComplete extends listActions.LoadComplete {
    constructor(public payload: { movies: IMovie[] }) {
        super(payload, ListDomain.SEARCH);
    }
}

export class SearchError extends listActions.LoadError {
    constructor(public payload: { error: string }) {
        super(payload, ListDomain.SEARCH);
    }
}

export class SearchMore extends listActions.LoadMore {
    constructor(public payload: {limit: number}) {
        super(payload, ListDomain.SEARCH);
    }
}

export class SearchMoreStart extends listActions.LoadMore {
    constructor(public payload: any) {
        super(payload, ListDomain.SEARCH);
    }
}

export class SearchMoreComplete extends listActions.LoadMoreComplete {
    constructor(public payload: { movies: IMovie[] }) {
        super(payload, ListDomain.SEARCH);
    }
}

export class SearchMoreError extends listActions.LoadMoreError {
    constructor(public payload: {error: string}) {
        super(payload, ListDomain.SEARCH);
    }
}

export class Select implements CustomAction {
    readonly type = SELECT;

    constructor(public payload: {imdbID: string}) {}
}

export class Load implements CustomAction {
    readonly type = LOAD;
    constructor(public payload: {imdbID: string}) {}
}

export class LoadComplete implements CustomAction {
    readonly type = LOAD_COMPLETE;
    constructor(public payload: { movie: IMovie }) {}
}

export class LoadError implements CustomAction {
    readonly type = LOAD_ERROR;
    constructor(public payload: { error: string }) {}
}

export type All =
    Search
    | SearchComplete
    | SearchError
    | Select
    | SearchMore
    | SearchMoreStart
    | SearchMoreComplete
    | SearchMoreError
    | Load
    | LoadComplete
    | LoadError;
