import {Action} from '@ngrx/store';
/**
 * Created by baunov on 14/11/2017.
 */
export interface CustomAction extends Action {
    payload?: any;
}
