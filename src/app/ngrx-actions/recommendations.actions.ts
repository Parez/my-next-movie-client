/**
 * Created by baunov on 14/11/2017.
 */
import { IMovie } from '../models/IMovie';
import {CustomAction} from './CustomAction';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {IMovieLoadOptions} from '../models/IMovieLoadOptions';
import {IRecommendation} from "../models/IRecommendation";

export const LOAD = '[Recommendations] Load';
export const LOAD_COMPLETE = '[Recommendations] Load Complete';
export const LOAD_ERROR = '[Recommendations] Load Error';
export const CLEAR = '[Recommendations] Clear';


export class Load implements CustomAction {
    readonly type = LOAD;
    constructor(public payload = null) {}
}

export class LoadComplete implements CustomAction {
    readonly type = LOAD_COMPLETE;
    constructor(public payload: { recs: IRecommendation[] }) {}
}

export class LoadError implements CustomAction {
    readonly type = LOAD_ERROR;
    constructor(public payload: { error: string }) {}
}

export class Clear implements CustomAction {
  readonly type = CLEAR;
  constructor(public payload = null) {}
}

export type All =
    Load
    | LoadComplete
    | LoadError
    | Clear;
