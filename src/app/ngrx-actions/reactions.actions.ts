/**
 * Created by baunov on 14/11/2017.
 */
import { IMovie } from '../models/IMovie';
import {CustomAction} from './CustomAction';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {IMovieLoadOptions} from '../models/IMovieLoadOptions';

export const COUNT_REACTIONS = '[Reactions] Count';
export const COUNT_REACTIONS_COMPLETE = '[Reactions] Count Complete';
export const COUNT_REACTIONS_ERROR = '[Reactions] Count Error';

export const LOAD_MOVIE_REACTION = '[Reactions] Load Movie Reaction';
export const LOAD_MOVIE_REACTION_COMPLETE = '[Reactions] Load Movie Reaction Complete';
export const LOAD_MOVIE_REACTION_ERROR = '[Reactions] Load Movie Reaction Error';

export const LIKE = '[Reactions] Like';
export const LIKE_COMPLETE = '[Reactions] Like Complete';
export const LIKE_ERROR = '[Reactions] Like Error';

export const DISLIKE = '[Reactions] Dislike';
export const DISLIKE_COMPLETE = '[Reactions] Dislike Complete';
export const DISLIKE_ERROR = '[Reactions] Dislike Error';

export const LOAD_LIKES = '[Reactions] Load Likes';
export const LOAD_LIKES_COMPLETE = '[Reactions] Load Likes Complete';
export const LOAD_LIKES_ERROR = '[Collection] Load Likes Error';

export const LOAD_DISLIKES = '[Reactions] Load Dislikes';
export const LOAD_DISLIKES_COMPLETE = '[Reactions] Load Dislikes Complete';
export const LOAD_DISLIKES_ERROR = '[Collection] Load Dislikes Error';
/*
export const LOAD_MORE = '[Collection] Load More';
export const LOAD_MORE_START = '[Collection] Load More Start';
export const LOAD_MORE_COMPLETE = '[Collection] Load More Complete';
export const LOAD_MORE_ERROR = '[Collection] Load More Error';*/

export class LoadMovieReaction implements CustomAction {
  readonly type = LOAD_MOVIE_REACTION;
  constructor(public payload: {movie: IMovie}) {}
}

export class LoadMovieReactionComplete implements CustomAction {
  readonly type = LOAD_MOVIE_REACTION_COMPLETE;
  constructor(public payload: {movie: IMovie, reaction: number}) {}
}

export class LoadMovieReactionError implements CustomAction {
  readonly type = LOAD_MOVIE_REACTION_ERROR;
  constructor(public payload: {error: string}) {}
}

export class CountReactions implements CustomAction {
  readonly type = COUNT_REACTIONS;
  constructor(public payload = null) {}
}

export class CountReactionsComplete implements CustomAction {
  readonly type = COUNT_REACTIONS_COMPLETE;
  constructor(public payload: number) {}
}

export class CountReactionsError implements CustomAction {
  readonly type = COUNT_REACTIONS_ERROR;
  constructor(public payload: {error: string}) {}
}

export class LoadLikes implements CustomAction {
    readonly type = LOAD_LIKES;
    constructor(public payload: IMovieLoadOptions = null) {}
}

export class LoadLikesComplete implements CustomAction {
    readonly type = LOAD_LIKES_COMPLETE;
    constructor(public payload: { movies: IMovie[] }) {}
}

export class LoadLikesError implements CustomAction {
    readonly type = LOAD_LIKES_ERROR;
    constructor(public payload: { error: string }) {}
}

export class LoadDislikes implements CustomAction {
  readonly type = LOAD_DISLIKES;
  constructor(public payload: IMovieLoadOptions = null) {}
}

export class LoadDislikesComplete implements CustomAction {
  readonly type = LOAD_DISLIKES_COMPLETE;
  constructor(public payload: { movies: IMovie[] }) {
    console.log(payload.movies);
  }
}

export class LoadDislikesError implements CustomAction {
  readonly type = LOAD_DISLIKES_ERROR;
  constructor(public payload: { error: string }) {}
}

export class Like implements CustomAction {
    readonly type = LIKE;
    constructor(public payload: {movie: IMovie}) {}
}

export class LikeComplete implements CustomAction {
    readonly type = LIKE_COMPLETE;
    constructor(public payload: {movie: IMovie, count: number}) {}
}

export class LikeError implements CustomAction {
    readonly type = LIKE_ERROR;
    constructor(public payload: {error: string}) {}
}

export class Dislike implements CustomAction {
    readonly type = DISLIKE;
    constructor(public payload: {movie: IMovie}) {}
}

export class DislikeComplete implements CustomAction {
    readonly type = DISLIKE_COMPLETE;
    constructor(public payload: {movie: IMovie, count: number}) {}
}

export class DislikeError implements CustomAction {
    readonly type = DISLIKE_ERROR;
    constructor(public payload: {error: string}) {}
}

export type All =
    LoadLikes
    | LoadLikesComplete
    | LoadLikesError
    | LoadDislikes
    | LoadDislikesComplete
    | LoadDislikesError
    | LoadMovieReaction
    | LoadMovieReactionComplete
    | LoadMovieReactionError
    | Like
    | LikeComplete
    | LikeError
    | Dislike
    | DislikeComplete
    | DislikeError
    | CountReactions
    | CountReactionsComplete
    | CountReactionsError;
