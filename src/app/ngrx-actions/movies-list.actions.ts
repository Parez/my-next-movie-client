import {CustomAction} from './CustomAction';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {IMovie} from '../models/IMovie';

export enum ListDomain {
    SEARCH = 'SEARCH',
    ACTOR = 'ACTOR',
    GENRE = 'GENRE',
    COLLECTION = 'COLLECTION'
}

export const LOAD = '[List] Load';
export const LOAD_COMPLETE = '[List] Load Complete';
export const LOAD_ERROR = '[List] Load Error';

export const LOAD_MORE = '[List] Load More';
export const LOAD_MORE_START = '[List] Load More Start';
export const LOAD_MORE_COMPLETE = '[List] Load More Complete';
export const LOAD_MORE_ERROR = '[List] Load More Error';


export class Load implements CustomAction {
    public readonly type: string;
    constructor(public payload: IMovieSearchOptions, public domain: ListDomain) {
        this.type = `${domain}_${LOAD}`;
    }
}

export class LoadComplete implements CustomAction {
    public readonly type: string;
    constructor(public payload: { movies: IMovie[] }, public domain: ListDomain) {
        this.type = `${domain}_${LOAD_COMPLETE}`;
    }
}

export class LoadError implements CustomAction {
    public readonly type: string;
    constructor(public payload: { error: string }, public domain: ListDomain) {
        this.type = `${domain}_${LOAD_ERROR}`;
    }
}

export class LoadMore implements CustomAction {
    public readonly type: string;
    constructor(public payload: {limit: number}, public domain: ListDomain) {
        this.type = `${domain}_${LOAD_MORE}`;
    }
}

export class LoadMoreStart implements CustomAction {
    public readonly type: string;
    constructor(public payload: any, public domain: ListDomain) {
        this.type = `${domain}_${LOAD_MORE_START}`;
    }
}

export class LoadMoreComplete implements CustomAction {
    public readonly type: string;
    constructor(public payload: { movies: IMovie[] }, public domain: ListDomain) {
        this.type = `${domain}_${LOAD_MORE_COMPLETE}`;
    }
}

export class LoadMoreError implements CustomAction {
    public readonly type: string;
    constructor(public payload: {error: string}, public domain: ListDomain) {
        this.type = `${domain}_${LOAD_MORE_ERROR}`;
    }
}

export function getActionType(type: string, domain: ListDomain): string {
    return `${domain}_${type}`;
}

export type All =
      Load
    | LoadComplete
    | LoadError
    | LoadMore
    | LoadMoreStart
    | LoadMoreComplete
    | LoadMoreError;
