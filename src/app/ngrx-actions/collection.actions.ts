/**
 * Created by baunov on 14/11/2017.
 */
import { IMovie } from '../models/IMovie';
import {CustomAction} from './CustomAction';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {IMovieLoadOptions} from '../models/IMovieLoadOptions';

export const ADD = '[Collection] Add';
export const ADD_COMPLETE = '[Collection] Add Complete';
export const ADD_ERROR = '[Collection] Add Error';

export const REMOVE = '[Collection] Remove';
export const REMOVE_COMPLETE = '[Collection] Remove Complete';
export const REMOVE_ERROR = '[Collection] Remove Error';

export const LOAD = '[Collection] Load';
export const LOAD_COMPLETE = '[Collection] Load Complete';
export const LOAD_ERROR = '[Collection] Load Error';

export const LOAD_MORE = '[Collection] Load More';
export const LOAD_MORE_START = '[Collection] Load More Start';
export const LOAD_MORE_COMPLETE = '[Collection] Load More Complete';
export const LOAD_MORE_ERROR = '[Collection] Load More Error';


export class Load implements CustomAction {
    readonly type = LOAD;
    constructor(public payload: IMovieLoadOptions) {}
}

export class LoadComplete implements CustomAction {
    readonly type = LOAD_COMPLETE;
    constructor(public payload: { movies: IMovie[] }) {}
}

export class LoadError implements CustomAction {
    readonly type = LOAD_ERROR;
    constructor(public payload: { error: string }) {}
}

export class LoadMore implements CustomAction {
    readonly type = LOAD_MORE;
    constructor(public payload: {limit: number}) {}
}

export class LoadMoreStart implements CustomAction {
    readonly type = LOAD_MORE_START;
    constructor(public payload: any) {}
}

export class LoadMoreComplete implements CustomAction {
    readonly type = LOAD_MORE_COMPLETE;
    constructor(public payload: { movies: IMovie[] }) {}
}

export class LoadMoreError implements CustomAction {
    readonly type = LOAD_MORE_ERROR;
    constructor(public payload: {error: string}) {}
}

export class Add implements CustomAction {
    readonly type = ADD;
    constructor(public payload: {movie: IMovie}) {}
}

export class AddComplete implements CustomAction {
    readonly type = ADD_COMPLETE;
    constructor(public payload: {movie: IMovie}) {}
}

export class AddError implements CustomAction {
    readonly type = ADD_ERROR;
    constructor(public payload: {error: string}) {}
}

export class Remove implements CustomAction {
    readonly type = REMOVE;
    constructor(public payload: {movie: IMovie}) {}
}

export class RemoveComplete implements CustomAction {
    readonly type = REMOVE_COMPLETE;
    constructor(public payload: {movie: IMovie}) {}
}

export class RemoveError implements CustomAction {
    readonly type = REMOVE_ERROR;
    constructor(public payload: {error: string}) {}
}

export type All =
    Load
    | LoadComplete
    | LoadError
    | LoadMore
    | LoadMoreStart
    | LoadMoreComplete
    | LoadMoreError
    | Add
    | AddComplete
    | AddError
    | Remove
    | RemoveComplete
    | RemoveError;
