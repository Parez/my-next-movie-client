/**
 * Created by baunov on 14/11/2017.
 */
import { IMovie } from '../models/IMovie';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import * as listActions from './movies-list.actions';
import {ListDomain} from './movies-list.actions';

export const SEARCH = listActions.getActionType(listActions.LOAD, ListDomain.ACTOR);
export const SEARCH_COMPLETE = listActions.getActionType(listActions.LOAD_COMPLETE, ListDomain.ACTOR);
export const SEARCH_ERROR = listActions.getActionType(listActions.LOAD_ERROR, ListDomain.ACTOR);
export const SEARCH_MORE = listActions.getActionType(listActions.LOAD_MORE, ListDomain.ACTOR);
export const SEARCH_MORE_START = listActions.getActionType(listActions.LOAD_MORE_START, ListDomain.ACTOR);
export const SEARCH_MORE_COMPLETE = listActions.getActionType(listActions.LOAD_MORE_COMPLETE, ListDomain.ACTOR);
export const SEARCH_MORE_ERROR = listActions.getActionType(listActions.LOAD_MORE_ERROR, ListDomain.ACTOR);

export class Search extends listActions.Load {
    constructor(public payload: IMovieSearchOptions) {
        super(payload, ListDomain.ACTOR);
    }
}

export class SearchComplete extends listActions.LoadComplete {
    constructor(public payload: { movies: IMovie[] }) {
        super(payload, ListDomain.ACTOR);
    }
}

export class SearchError extends listActions.LoadError {
    constructor(public payload: { error: string }) {
        super(payload, ListDomain.ACTOR);
    }
}

export class SearchMore extends listActions.LoadMore {
    constructor(public payload: {limit: number}) {
        super(payload, ListDomain.ACTOR);
    }
}

export class SearchMoreStart extends listActions.LoadMore {
    constructor(public payload: any) {
        super(payload, ListDomain.ACTOR);
    }
}

export class SearchMoreComplete extends listActions.LoadMoreComplete {
    constructor(public payload: { movies: IMovie[] }) {
        super(payload, ListDomain.ACTOR);
    }
}

export class SearchMoreError extends listActions.LoadMoreError {
    constructor(public payload: {error: string}) {
        super(payload, ListDomain.ACTOR);
    }
}

export type All =
    Search
    | SearchComplete
    | SearchError
    | SearchMore
    | SearchMoreStart
    | SearchMoreComplete
    | SearchMoreError;
