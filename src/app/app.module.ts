import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeModule } from './modules/home/home.module';
import { FacebookModule } from 'ngx-facebook';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import {MoviesService} from './services/movies.service';
import {MovieModule} from './modules/movie/movie.module';
import {SharedModule} from './shared/shared.module';
import {EffectsModule} from '@ngrx/effects';
import {SearchEffects} from './ngrx-effects/search.effects';
import {StoreModule} from '@ngrx/store';
import {InitialState, reducers} from './ngrx-reducers/index';
import {CollectionModule} from './modules/collection/collection.module';
import {CollectionEffects} from './ngrx-effects/collection.effects';
import {RecommendationsModule} from './modules/recommendations/recommendations.module';
import {MovieEffects} from './ngrx-effects/movie.effects';
import {ActorModule} from './modules/actor/actor.module';
import {ActorEffects} from './ngrx-effects/actor.effects';
import {GenreEffects} from './ngrx-effects/genre.effects';
import {GenreModule} from './modules/genre/genre.module';
import {AuthEffects} from './ngrx-effects/auth.effects';
import {LoginModule} from './modules/login/login.module';
import {metaReducers} from './ngrx-reducers/meta-reducers/index';
import {ErrorsEffects} from './ngrx-effects/errors.effects';
import {ReactionsModule} from './modules/reactions/reactions.module';
import {ReactionsEffects} from './ngrx-effects/reactions.effects';
import {RecommendationsEffects} from './ngrx-effects/recommendations.effects';
import {PopupService} from './services/popup.service';
import {TouchService} from './services/touch.service';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    CollectionModule,
    RecommendationsModule,
    ReactionsModule,
    MovieModule,
    ActorModule,
    GenreModule,
    FacebookModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    LoginModule,
    EffectsModule.forRoot([
        SearchEffects,
        CollectionEffects,
        MovieEffects,
        ActorEffects,
        GenreEffects,
        AuthEffects,
        ErrorsEffects,
        ReactionsEffects,
        RecommendationsEffects
    ]),
    StoreModule.forRoot(reducers, {
        initialState: InitialState,
        metaReducers
    })
  ],
  providers: [
      MoviesService,
      PopupService,
      TouchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
