/**
 * Created by baunov on 14/11/2017.
 */

export interface IMovieLoadOptions {
    skip?: number;
    limit?: number;
}
