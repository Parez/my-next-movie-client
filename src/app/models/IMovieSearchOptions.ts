/**
 * Created by baunov on 14/11/2017.
 */
import {IMovieLoadOptions} from './IMovieLoadOptions';

export interface IMovieSearchOptions extends IMovieLoadOptions {
    search?: string;
    field?: string;
    sortOn?: string;
    sortOrder?: number;
}
