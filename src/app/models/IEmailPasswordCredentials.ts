export interface IEmailPasswordCredentials {
    email: string;
    password: string;
}