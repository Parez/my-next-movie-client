import {IMovie} from './IMovie';

export interface IPopupData {
  movie: IMovie;
  posRight: number;
  posTop: number;
  posLeft?: number;
  posBottom?: number;
  width?: number;
  height?: number;
}
