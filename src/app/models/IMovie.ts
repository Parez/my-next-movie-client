import {IMovieBase} from './IMovieBase';
/**
 * Created by baunov on 09/11/2017.
 */
export interface IMovie extends IMovieBase {
    Year: number;
    Poster: string;
    imdbRating: number;
    Genres: string[];
    Plot: string;
    Countries?: string;
    Actors: string[];
    Directors?: string;
    Writers?: string;
    Productions?: string;
    YoutubeTrailer: string;
    BigPoster?: string;
}
