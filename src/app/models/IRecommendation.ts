/**
 * Created by baunov on 09/11/2017.
 */
import {IMovie} from './IMovie';

export interface IRecommendation {
    TitleScore: number;
    DecadeScore: number;
    GenresScore: number;
    ActorsScore: number;
    PlotScore: number;
    CountriesScore: number;
    WritersScore: number;
    DirectorsScore: number;
    ProductionsScore: number;
    score: number;
    movie: IMovie;
}
