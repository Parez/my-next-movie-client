/**
 * Created by baunov on 14/11/2017.
 */

export interface IMovieBase {
    imdbID: number;
    Title: string;
}