import {ListDomain} from '../ngrx-actions/movies-list.actions';
import * as fromList from './movies-list.reducer';

export function searchReducer(state, action): fromList.ListState {
    return fromList.createReducer(ListDomain.SEARCH)(state, action);
}
