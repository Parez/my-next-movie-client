import * as MoviesActions from '../ngrx-actions/search.actions';

export interface MovieState {
    id: number;
    loading: boolean;
    loaded: boolean;
    error: string;
}

export const initialState: MovieState = {
    id: 0,
    loading: false,
    loaded: false,
    error: ''
};

export function reducer(state = initialState, action: MoviesActions.All): MovieState {
    switch (action.type) {
        case MoviesActions.LOAD: {
            return {
                ...state,
                loading: true,
                loaded: false,
                error: ''
            };
        }
        case MoviesActions.LOAD_COMPLETE: {
            return {
                ...state,
                id: action.payload.movie.imdbID,
                loading: false,
                loaded: true,
                error: ''
            };
        }
        case MoviesActions.LOAD_ERROR: {
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.payload.error
            };
        }

        default: {
            return state;
        }
    }
}

export const getId = (state: MovieState) => state.id;

export const getLoading = (state: MovieState) => state.loading;

export const getLoaded = (state: MovieState) => state.loaded;

export const getError = (state: MovieState) => state.error;
