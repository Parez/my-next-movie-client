import * as ReactionsActions from '../ngrx-actions/reactions.actions';

export interface ReactionsState {
  loading: boolean;
  loaded: boolean;
  error: string;
  reactions: {[id: string]: number};
}

export const initialState: ReactionsState = {
  loading: false,
  loaded: false,
  error: '',
  reactions: {}
};

export function reducer(state = initialState, action: ReactionsActions.All): ReactionsState {
  switch (action.type) {
    case ReactionsActions.LOAD_LIKES_COMPLETE: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: '',
        reactions: Object.assign(
          {},
          state.reactions,
          action.payload.movies
            .map(movie => ({movie: movie.imdbID, reaction: 1}))
            .reduce((acc, r) => Object.assign({}, acc, {[r.movie]: r.reaction}), {})
        )
      };
    }
    case ReactionsActions.LOAD_DISLIKES_COMPLETE: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: '',
        reactions: Object.assign(
          {},
          state.reactions,
          action.payload.movies
            .map(movie => ({movie: movie.imdbID, reaction: -1}))
            .reduce((acc, r) => Object.assign({}, acc, {[r.movie]: r.reaction}), {})
        )
      };
    }
    case ReactionsActions.LOAD_MOVIE_REACTION:
    case ReactionsActions.DISLIKE:
    case ReactionsActions.LIKE: {
      return {
        ...state,
        loading: true,
        loaded: false,
        error: ''
      };
    }
    case ReactionsActions.LOAD_MOVIE_REACTION_COMPLETE: {
      return {
        ...state,
        loading: false,
        loaded: true,
        reactions: Object.assign({}, state.reactions, {[action.payload.movie.imdbID]: action.payload.reaction})
      };
    }
    case ReactionsActions.LIKE_COMPLETE: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: '',
        reactions: Object.assign({}, state.reactions, {[action.payload.movie.imdbID]: 1})
      };
    }
    case ReactionsActions.DISLIKE_COMPLETE: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: '',
        reactions: Object.assign({}, state.reactions, {[action.payload.movie.imdbID]: -1})
      };
    }
    case ReactionsActions.DISLIKE_ERROR:
    case ReactionsActions.LIKE_ERROR: {
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload.error
      };
    }
    default: {
      return state;
    }
  }
}

export const getLikedIds = (state: ReactionsState) => Object.keys(state.reactions).filter(key => state.reactions[key] === 1);
export const getDislikedIds = (state: ReactionsState) => Object.keys(state.reactions).filter(key => state.reactions[key] === -1);

export const getMovieReactions = (state: ReactionsState) => state.reactions;

export const getLoading = (state: ReactionsState) => state.loading;

export const getLoaded = (state: ReactionsState) => state.loaded;

export const getError = (state: ReactionsState) => state.error;
