import * as CollectionActions from '../ngrx-actions/collection.actions';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {IMovieLoadOptions} from '../models/IMovieLoadOptions';
import {CustomAction} from '../ngrx-actions/CustomAction';

export const AMOUNT_PER_PAGE = 30;

export interface CollectionState {
    ids: number[];
    loading: boolean;
    loaded: boolean;
    allLoaded: boolean;
    error: string;
    query: IMovieLoadOptions;
    pageNumber: number;
}

export const initialState: CollectionState = {
    ids: [],
    loading: false,
    loaded: false,
    allLoaded: false,
    error: '',
    query: {},
    pageNumber: 0
};

export function reducer(state = initialState, action: CustomAction): CollectionState {
    switch (action.type) {
        case CollectionActions.ADD_COMPLETE: {
            return {
                ...state,
                ids: state.ids.concat(action.payload.movie.imdbID)
            };
        }
        case CollectionActions.REMOVE_COMPLETE: {
            return {
                ...state,
                ids: state.ids.filter(id => id !== action.payload.movie.imdbID)
            };
        }
        case CollectionActions.ADD_ERROR: {
            return {
                ...state,
                error: action.payload.error
            };
        }
        case CollectionActions.REMOVE_ERROR: {
            return {
                ...state,
                error: action.payload.error
            };
        }
        case CollectionActions.LOAD_MORE_START: {
            return {
                ...state,
                loading: true,
                loaded: false,
                error: '',
                pageNumber: state.pageNumber + 1
            };
        }
        case CollectionActions.LOAD_MORE_COMPLETE: {
            return {
                ...state,
                ids: state.ids.concat(action.payload.movies.map(movie => movie.imdbID)),
                loading: false,
                loaded: true,
                error: '',
                allLoaded: action.payload.movies.length < AMOUNT_PER_PAGE
            };
        }
        case CollectionActions.LOAD: {
            return {
                ...state,
                loading: true,
                loaded: false,
                error: '',
                query: action.payload,
                pageNumber: 0
            };
        }

        case CollectionActions.LOAD_COMPLETE: {
            return {
                ids: action.payload.movies.map(movie => movie.imdbID),
                loading: false,
                loaded: true,
                error: '',
                query: state.query,
                pageNumber: state.pageNumber,
                allLoaded: action.payload.movies.length < AMOUNT_PER_PAGE
            };
        }

        case CollectionActions.LOAD_MORE_ERROR:
        case CollectionActions.LOAD_ERROR: {
            return {
                ...state,
                loading: false,
                loaded: false,
                allLoaded: false,
                error: action.payload.error,
            };
        }

        default: {
            return state;
        }
    }
}

export const getIds = (state: CollectionState) => state.ids;

export const getQuery = (state: CollectionState) => state.query;

export const getLoading = (state: CollectionState) => state.loading;

export const getLoaded = (state: CollectionState) => state.loaded;

export const getAllLoaded = (state: CollectionState) => state.allLoaded;

export const getError = (state: CollectionState) => state.error;
