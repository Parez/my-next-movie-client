/**
 * Created by baunov on 14/11/2017.
 */
import {ActionReducerMap, createSelector} from '@ngrx/store';
import * as fromMovies from './movies.reducer';
import * as fromMovie from './movie.reducer';
import * as fromList from './movies-list.reducer';
import * as fromCollection from './collection.reducer';
import * as fromAuth from './auth.reducer';
import * as fromReactions from './reactions.reducer';
import * as fromReactionsCounter from './reactions-counter.reducer';
import * as fromRecommendations from './recommendations.reducer';
import {searchReducer} from './search.reducer';
import {actorReducer} from './actor.reducer';
import {genreReducer} from './genre.reducer';


export interface State {
    movies: fromMovies.MoviesState;
    movie: fromMovie.MovieState;
    search: fromList.ListState;
    actor: fromList.ListState;
    genre: fromList.ListState;
    collection: fromCollection.CollectionState;
    auth: fromAuth.AuthState;
    reactions: fromReactions.ReactionsState;
    reactions_counter: fromReactionsCounter.ReactionsCounterState;
    recommendations: fromRecommendations.RecommendationsState;
}

export const InitialState: State = {
    movies: fromMovies.initialState,
    movie: fromMovie.initialState,
    search: fromList.initialState,
    actor: fromList.initialState,
    genre: fromList.initialState,
    collection: fromCollection.initialState,
    auth: fromAuth.initialState,
    reactions: fromReactions.initialState,
    reactions_counter: fromReactionsCounter.initialState,
    recommendations: fromRecommendations.initialState
};


export const reducers: ActionReducerMap<State> = {
    movies: fromMovies.reducer,
    movie: fromMovie.reducer,
    search: searchReducer,
    actor: actorReducer,
    genre: genreReducer,
    collection: fromCollection.reducer,
    auth: fromAuth.reducer,
    reactions: fromReactions.reducer,
    reactions_counter: fromReactionsCounter.reducer,
    recommendations: fromRecommendations.reducer
};

// Recommendations

export const getRecommendationsState = (state: State) => state.recommendations;

export const getRecommendationsIds = createSelector(
  getRecommendationsState,
  fromRecommendations.getIds
);

export const getRecommendationsEntities = createSelector(
  getRecommendationsState,
  fromRecommendations.getEntities
);

export const getRecommendations = createSelector(
  getRecommendationsEntities,
  getRecommendationsIds,
  (recs, ids) => ids.map(id => recs[id])
);

export const getRecommendationsLoading = createSelector(
  getRecommendationsState,
  fromRecommendations.getLoading
);

export const getRecommendationsLoaded = createSelector(
  getRecommendationsState,
  fromRecommendations.getLoaded
);

export const getRecommendationsError = createSelector(
  getRecommendationsState,
  fromRecommendations.getError
);

// Reactions Counter

export const getReactionsCounterState = (state: State) => state.reactions_counter;

export const getReactionsCounterCount = createSelector(
  getReactionsCounterState,
  fromReactionsCounter.getCount
);

export const getReactionsCounterCountLeft = createSelector(
  getReactionsCounterState,
  fromReactionsCounter.getCountLeft
);

export const getReactionsCounterLoading = createSelector(
  getReactionsCounterState,
  fromReactionsCounter.getLoading
);

export const getReactionsCounterLoaded = createSelector(
  getReactionsCounterState,
  fromReactionsCounter.getLoaded
);

export const getReactionsCounterError = createSelector(
  getReactionsCounterState,
  fromReactionsCounter.getError
);

// Auth

export const getAuthState = (state: State) => state.auth;

export const getAuthUser = createSelector(
    getAuthState,
    fromAuth.getUser
);

export const getAuthLoading = createSelector(
    getAuthState,
    fromAuth.getLoading
);

export const getAuthLoggedIn = createSelector(
    getAuthState,
    fromAuth.getLoggedIn
);

export const getAuthError = createSelector(
    getAuthState,
    fromAuth.getError
);

// Movies

export const getMoviesState = (state: State) => state.movies;

export const {
    selectIds: getMovieIds,
    selectEntities: getMovieEntities,
    selectAll: getAllMovies,
    selectTotal: getTotalMovies,
} = fromMovies.adapter.getSelectors(getMoviesState);


// Reactions

export const getReactionsState = (state: State) => state.reactions;

export const getReactions = createSelector(
  getReactionsState,
  fromReactions.getMovieReactions
);

export const getMovieReaction = (movieId: string) => {
  return createSelector(
    getReactions,
    items => items[movieId]
  );
};

export const getReactionsLoading = createSelector(
  getReactionsState,
  fromReactions.getLoading
);

export const getReactionsLoaded = createSelector(
  getReactionsState,
  fromReactions.getLoaded
);

export const getReactionsError = createSelector(
  getReactionsState,
  fromReactions.getError
);

export const getReactionsLikedIds = createSelector(
  getReactionsState,
  fromReactions.getLikedIds
);

export const getReactionsDislikedIds = createSelector(
  getReactionsState,
  fromReactions.getDislikedIds
);

export const getLikedMovies = createSelector(
  getMovieEntities,
  getReactionsLikedIds,
  (movies, likedIds) => likedIds.map(id => movies[id])
);

export const getDislikedMovies = createSelector(
  getMovieEntities,
  getReactionsDislikedIds,
  (movies, dislikedIds) => dislikedIds.map(id => movies[id])
);

// Movie

export const getMovieState = (state: State) => state.movie;

export const getMovieId = createSelector(
    getMovieState,
    fromMovie.getId
);

export const getMovieLoading = createSelector(
    getMovieState,
    fromMovie.getLoading
);

export const getMovieLoaded = createSelector(
    getMovieState,
    fromMovie.getLoaded
);

export const getMovieError = createSelector(
    getMovieState,
    fromMovie.getError
);

export const getMovie = createSelector(
    getMovieEntities,
    getMovieId,
    (movies, movieId) => {
        return movies[movieId];
    }
);


// Collection

export const getCollectionState = (state: State) => state.collection;

export const getCollectionMovieIds = createSelector(
    getCollectionState,
    fromCollection.getIds
);

export const getCollectionLoading = createSelector(
    getCollectionState,
    fromCollection.getLoading
);

export const getCollectionLoaded = createSelector(
    getCollectionState,
    fromCollection.getLoaded
);

export const getCollectionAllLoaded = createSelector(
    getCollectionState,
    fromCollection.getAllLoaded
);
export const getCollectionError = createSelector(
    getCollectionState,
    fromCollection.getError
);

export const getCollectionMovies = createSelector(
    getMovieEntities,
    getCollectionMovieIds,
    (movies, collectionIds) => {
        return collectionIds.map(id => movies[id]);
    }
);

// Search

export const getSearchState = (state: State) => state.search;

export const getSearchMovieIds = createSelector(
    getSearchState,
    fromList.getIds
);

export const getSearchQuery = createSelector(
    getSearchState,
    fromList.getQuery
);
export const getSearchLoading = createSelector(
    getSearchState,
    fromList.getLoading
);
export const getSearchLoaded = createSelector(
    getSearchState,
    fromList.getLoaded
);
export const getSearchAllLoaded = createSelector(
    getSearchState,
    fromList.getAllLoaded
);
export const getSearchError = createSelector(
    getSearchState,
    fromList.getError
);

export const getSearchResults = createSelector(
    getMovieEntities,
    getSearchMovieIds,
    (movies, searchIds) => {
        return searchIds.map(id => movies[id]);
    }
);

// Actor

export const getActorState = (state: State) => state.actor;

export const getActorMovieIds = createSelector(
    getActorState,
    fromList.getIds
);

export const getActorQuery = createSelector(
    getActorState,
    fromList.getQuery
);
export const getActorLoading = createSelector(
    getActorState,
    fromList.getLoading
);
export const getActorLoaded = createSelector(
    getActorState,
    fromList.getLoaded
);
export const getActorAllLoaded = createSelector(
    getActorState,
    fromList.getAllLoaded
);
export const getActorError = createSelector(
    getActorState,
    fromList.getError
);

export const getActorResults = createSelector(
    getMovieEntities,
    getActorMovieIds,
    (movies, searchIds) => {
        console.log(movies, searchIds);
        return searchIds.map(id => movies[id]);
    }
);


// Genre

export const getGenreState = (state: State) => state.genre;

export const getGenreMovieIds = createSelector(
    getGenreState,
    fromList.getIds
);

export const getGenreQuery = createSelector(
    getGenreState,
    fromList.getQuery
);
export const getGenreLoading = createSelector(
    getGenreState,
    fromList.getLoading
);
export const getGenreLoaded = createSelector(
    getGenreState,
    fromList.getLoaded
);
export const getGenreAllLoaded = createSelector(
    getGenreState,
    fromList.getAllLoaded
);
export const getGenreError = createSelector(
    getGenreState,
    fromList.getError
);

export const getGenreResults = createSelector(
    getMovieEntities,
    getGenreMovieIds,
    (movies, searchIds) => {
        console.log(movies, searchIds);
        return searchIds.map(id => movies[id]);
    }
);
