import * as RecommendationActions from '../ngrx-actions/recommendations.actions';
import {IRecommendation} from '../models/IRecommendation';

export interface RecommendationsState {
    ids: number[];
    entities: {[id: string]: IRecommendation};
    loading: boolean;
    loaded: boolean;
    error: string;
}

export const initialState: RecommendationsState = {
    ids: [],
    entities: {},
    loading: false,
    loaded: false,
    error: ''
};

export function reducer(state = initialState, action: RecommendationActions.All): RecommendationsState {
    switch (action.type) {
        case RecommendationActions.LOAD: {
            return {
                ...state,
                loading: true,
                loaded: false,
                error: '',
            };
        }

        case RecommendationActions.LOAD_COMPLETE: {
            return {
                ids: action.payload.recs.map(rec => rec.movie.imdbID),
                loading: false,
                loaded: true,
                error: '',
                entities: action.payload.recs.length > 0 ?
                  action.payload.recs.reduce((acc, rec) => Object.assign({}, acc, {[rec.movie.imdbID]: rec}), {}) :
                  state.entities
            };
        }
        case RecommendationActions.LOAD_ERROR: {
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.payload.error,
            };
        }
        case RecommendationActions.CLEAR: {
          console.log(initialState);
          return initialState;
        }
        default: {
            return state;
        }
    }
}

export const getIds = (state: RecommendationsState) => state.ids;
export const getEntities = (state: RecommendationsState) => state.entities;

export const getLoading = (state: RecommendationsState) => state.loading;

export const getLoaded = (state: RecommendationsState) => state.loaded;

export const getError = (state: RecommendationsState) => state.error;
