import * as ListActions from '../ngrx-actions/movies-list.actions';
import {IMovieSearchOptions} from '../models/IMovieSearchOptions';
import {getActionType, ListDomain} from '../ngrx-actions/movies-list.actions';

export const AMOUNT_PER_PAGE = 30;

export interface ListState {
    ids: number[];
    loading: boolean;
    loaded: boolean;
    allLoaded: boolean;
    error: string;
    query: IMovieSearchOptions;
    pageNumber: number;
}

export const initialState: ListState = {
    ids: [],
    loading: false,
    loaded: false,
    allLoaded: false,
    error: '',
    query: {search: ''},
    pageNumber: 0
};

export function createReducer(domain: ListDomain) {
    return function reducer(state = initialState, action: ListActions.All): ListState {
        if (action.domain !== domain) return state;
        switch (action.type) {
            case getActionType(ListActions.LOAD_MORE_START, domain): {
                return {
                    ...state,
                    loading: true,
                    loaded: false,
                    error: '',
                    pageNumber: state.pageNumber + 1
                };
            }
            case getActionType(ListActions.LOAD_MORE_COMPLETE, domain): {
                return {
                    ...state,
                    ids: state.ids.concat(action.payload.movies.map(movie => movie.imdbID)),
                    loading: false,
                    loaded: true,
                    error: '',
                    allLoaded: action.payload.movies.length < AMOUNT_PER_PAGE
                };
            }
            case getActionType(ListActions.LOAD, domain): {
                const query = action.payload.search;

                if (query === '') {
                    return {
                        ids: [],
                        loading: false,
                        loaded: false,
                        error: '',
                        query: action.payload,
                        pageNumber: state.pageNumber,
                        allLoaded: false
                    };
                }

                return {
                    ...state,
                    ids: [],
                    loading: true,
                    loaded: false,
                    error: '',
                    query: action.payload,
                    pageNumber: 0
                };
            }

            case getActionType(ListActions.LOAD_COMPLETE, domain): {
                return {
                    ...state,
                    ids: action.payload.movies.map(movie => movie.imdbID),
                    loading: false,
                    loaded: true,
                    error: '',
                    pageNumber: state.pageNumber,
                    allLoaded: action.payload.movies.length < AMOUNT_PER_PAGE
                };
            }

            case getActionType(ListActions.LOAD_MORE_ERROR, domain):
            case getActionType(ListActions.LOAD_ERROR, domain): {
                return {
                    ...state,
                    loading: false,
                    loaded: false,
                    allLoaded: false,
                    error: action.payload.error,
                };
            }

            default: {
                return state;
            }
        }
    };
}

export const getIds = (state: ListState) => state.ids;

export const getPageIds = (state: ListState) => state.ids.slice(-AMOUNT_PER_PAGE);

export const getQuery = (state: ListState) => state.query;

export const getLoading = (state: ListState) => state.loading;
export const getLoaded = (state: ListState) => state.loaded;
export const getAllLoaded = (state: ListState) => state.allLoaded;

export const getError = (state: ListState) => state.error;
