import {MetaReducer} from '@ngrx/store';
import {localStorageReducer} from './local-storage.reducer';
import {logoutReducer} from './logout.reducer';

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageReducer, logoutReducer];
