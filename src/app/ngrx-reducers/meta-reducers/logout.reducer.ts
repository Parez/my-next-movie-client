import {LOGOUT} from '../../ngrx-actions/auth.actions';

// reducer for resetting state to initial once logged out

export function logoutReducer(reducer) {
    return function (state, action) {
        return reducer(action.type === LOGOUT ? undefined : state, action);
    };
}
