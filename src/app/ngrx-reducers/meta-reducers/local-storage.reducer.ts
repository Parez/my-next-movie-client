import { localStorageSync } from 'ngrx-store-localstorage';
import {ActionReducer} from '@ngrx/store';

// Reducer for syncing parts of the state (auth) with localStorage

export function localStorageReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({keys: ['auth'], rehydrate: true})(reducer);
}
