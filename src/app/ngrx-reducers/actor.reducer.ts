import {ListDomain} from '../ngrx-actions/movies-list.actions';
import * as fromList from './movies-list.reducer';

export function actorReducer(state, action): fromList.ListState {
    return fromList.createReducer(ListDomain.ACTOR)(state, action);
}
