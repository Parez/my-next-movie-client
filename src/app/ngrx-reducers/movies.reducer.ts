/**
 * Created by baunov on 14/11/2017.
 */
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { IMovie } from '../models/IMovie';
import * as SearchActions from '../ngrx-actions/search.actions';
import * as ActorActions from '../ngrx-actions/actor.actions';
import * as GenreActions from '../ngrx-actions/genre.actions';
import * as CollectionActions from '../ngrx-actions/collection.actions';
import * as ReactionsActions from '../ngrx-actions/reactions.actions';
import * as RecommendationActions from '../ngrx-actions/recommendations.actions';
import {CustomAction} from '../ngrx-actions/CustomAction';




/**
 * @ngrx/entity provides a predefined interface for handling
 * a structured dictionary of records. This interface
 * includes an array of ids, and a dictionary of the provided
 * model type by id. This interface is extended to include
 * any additional interface properties.
 */
export interface MoviesState extends EntityState<IMovie> {
    selectedMovieId: string | null;
}

/**
 * createEntityAdapter creates many an object of helper
 * functions for single or multiple operations
 * against the dictionary of records. The configuration
 * object takes a record id selector function and
 * a sortComparer option which is set to a compare
 * function if the records are to be sorted.
 */
export const adapter: EntityAdapter<IMovie> = createEntityAdapter<IMovie>({
    selectId: (movie: IMovie) => movie.imdbID,
    sortComparer: false,
});

/** getInitialState returns the default initial state
 * for the generated entity state. Initial state
 * additional properties can also be defined.
 */
export const initialState: MoviesState = {
    selectedMovieId: null,
    ids: [],
    entities: {}
};

export function reducer(
    state = initialState,
    action: CustomAction
): MoviesState {
    console.log(action);
    switch (action.type) {
        case SearchActions.SEARCH_MORE_COMPLETE:
        case SearchActions.SEARCH_COMPLETE:
        case ActorActions.SEARCH_MORE_COMPLETE:
        case ActorActions.SEARCH_COMPLETE:
        case GenreActions.SEARCH_MORE_COMPLETE:
        case GenreActions.SEARCH_COMPLETE:
        case ReactionsActions.LOAD_LIKES_COMPLETE:
        case ReactionsActions.LOAD_DISLIKES_COMPLETE:
        case CollectionActions.LOAD_COMPLETE: {
            console.log(action.payload.movies);
            return adapter.addMany(action.payload.movies, state);
        }
        case SearchActions.LOAD_COMPLETE: {
            return adapter.addOne(action.payload.movie, state);
        }
        case RecommendationActions.LOAD_COMPLETE: {
          return adapter.addMany(action.payload.recs.map((rec) => rec.movie), state);
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */

export const getSelectedId = (state: MoviesState) => state.selectedMovieId;
