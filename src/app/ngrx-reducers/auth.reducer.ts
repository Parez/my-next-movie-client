import * as AuthActions from '../ngrx-actions/auth.actions';
import {AuthService} from '../services/auth.service';

export interface AuthState {
    user: any;
    loggedIn: boolean;
    loading: boolean;
    error: string;
}

export const initialState: AuthState = {
    user: null,
    loggedIn: false,
    loading: false,
    error: ''
};

export function reducer(state = initialState, action: AuthActions.All): AuthState {
    switch (action.type) {
        case AuthActions.REGISTER:
        case AuthActions.LOGIN_EMAIL: {
            return {
                ...state,
                loading: true,
                error: null
            };
        }
        case AuthActions.LOAD_USER_COMPLETE:
        case AuthActions.REGISTER_COMPLETE:
        case AuthActions.LOGIN_EMAIL_COMPLETE: {
            return {
                ...state,
                loading: false,
                user: action.payload.user,
                loggedIn: true
            };
        }
        case AuthActions.CHECK_AUTH_ERROR:
        case AuthActions.REGISTER_ERROR:
        case AuthActions.LOGIN_EMAIL_ERROR: {
            return {
                ...state,
                loading: false,
                user: null,
                loggedIn: false,
                error: action.payload.error
            };
        }
        case AuthActions.LOGOUT_COMPLETE: {
            return {
                ...state,
                loading: false,
                loggedIn: false,
                user: null
            };
        }
        default: {
            return state;
        }
    }
}

export const getUser = (state: AuthState) => state.user;

export const getLoading = (state: AuthState) => state.loading;

export const getLoggedIn = (state: AuthState) => state.loggedIn;

export const getError = (state: AuthState) => state.error;
