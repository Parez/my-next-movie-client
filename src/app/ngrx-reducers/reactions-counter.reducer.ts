import * as ReactionsActions from '../ngrx-actions/reactions.actions';
export const MIN_REACTIONS = 10;
export const EVERY_REACTIONS = 5;

export interface ReactionsCounterState {
  count: number;
  loading: boolean;
  loaded: boolean;
  error: string;
}

export const initialState: ReactionsCounterState = {
  count: 0,
  loading: false,
  loaded: false,
  error: ''
};

export function reducer(state = initialState, action: ReactionsActions.All): ReactionsCounterState {
  switch (action.type) {
    case ReactionsActions.DISLIKE_COMPLETE:
    case ReactionsActions.LIKE_COMPLETE: {
      return {
        ...state,
        count: action.payload.count
      };
    }
    case ReactionsActions.COUNT_REACTIONS: {
      return {
        ...state,
        loading: true,
        loaded: false,
        error: ''
      };
    }
    case ReactionsActions.COUNT_REACTIONS_COMPLETE: {
      return {
        ...state,
        count: action.payload,
        loading: false,
        loaded: true,
        error: ''
      };
    }
    case ReactionsActions.COUNT_REACTIONS_ERROR: {
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload.error
      };
    }
    default: {
      return state;
    }
  }
}

export const getCountLeft = (state: ReactionsCounterState) => {
  const curReactions = state.count;
  if (curReactions < MIN_REACTIONS) {
    return MIN_REACTIONS - curReactions;
  } else {
    return MIN_REACTIONS + EVERY_REACTIONS * Math.floor((curReactions - MIN_REACTIONS) / EVERY_REACTIONS + 1)  - curReactions;
  }
};

export const getCount = (state: ReactionsCounterState) => state.count;

export const getLoading = (state: ReactionsCounterState) => state.loading;

export const getLoaded = (state: ReactionsCounterState) => state.loaded;

export const getError = (state: ReactionsCounterState) => state.error;
