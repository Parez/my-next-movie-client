# Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

## Install the dependencies

Make sure you have node and npm installed. This project was tested on version of node 8.*.

In the root directory run `npm install` to install all the dependencies.

## Development server

Run `npm start` to start the development server on port 4201. The app will automatically reload if you change any of the source files.

## Production build

Run `ng build --prod` to build application for production. The output files will be in the dist folder.

## NodeJS Server API

In development mode the application will access API at http://localhost:3000/api

In production mode it will access API at https://api.my-next-movie.com/api
